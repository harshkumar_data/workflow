import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { TodolistPostgresModule } from './todolist-postgres/todolist-postgres.module';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports: [
    TypeOrmModule.forRootAsync({
      imports: [
        ConfigModule.forRoot({
          isGlobal: true,
          envFilePath: ['.local.env']
      })],
      useFactory: (configService: ConfigService) => ({
        type: 'postgres', 
        host: configService.get("HOST"),
        port: +configService.get<number>("PORT"),
        username: configService.get("USERNAME"),
        password: configService.get("PASSWORD"),
        database: configService.get("DATABASE"),
        synchronize: configService.get<boolean>("SYNCHRONIZE"),
        entities: [__dirname + '/**/*.entity{.ts, .js}'],
        logging: true
      }),
      inject: [ConfigService]

    }),
    TodolistPostgresModule,

  ],


  controllers: [AppController],
  providers: [AppService],
})
export class AppModule { }
