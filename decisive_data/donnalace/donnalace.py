from xmlrpc.server import SimpleXMLRPCDispatcher
from requests import Session
from bs4 import BeautifulSoup as BS
from lxml import html
import pandas as pd
import csv
import re
import json

s = Session()

f = open('donnalace_remain.csv','w',encoding='utf-8',newline='')
row = csv.writer(f)
row.writerow(['URL','PRODUCT_CODE','COLORS','SIZES','CATEGORY'])

s.headers = {'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:96.0) Gecko/20100101 Firefox/96.01', 'Accept-Encoding': 'gzip, deflate', 'Accept': '*/*', 'Connection': 'keep-alive'}

# data = []
def crawl(link, category):
    r = s.get(link)
    print(r.url, r.status_code)
    soup = BS(r.content,'html.parser')
    tree = html.fromstring(r.content)
    # title = ''.join(tree.xpath('//h1[@class="product-title"]//text()')).strip()
    # brand = ''.join(tree.xpath('//div[@id="content"]//ul[@class="list-unstyled"]//li[contains(string(),"Brand:")]//a//text()')).strip()
    # stock = ''.join(tree.xpath('//div[@id="content"]//ul[@class="list-unstyled"]//li[contains(string(),"Availability:")]//text()')).split(': ')[1]
    # des = ''.join(tree.xpath('//div[@id="content"]//div[@class="panel-body"]//text()')).strip()
    # discount_per = ''.join(tree.xpath('//ul[@class="list-unstyled price"]//li//h3//text()')).strip()
    sku = ''.join(tree.xpath('//div[@id="content"]//ul[@class="list-unstyled"]//li[contains(string(),"Product Code : ")]//span//text()')).strip()
    colors = '| '.join(tree.xpath('//div[@id="content"]//div[@id="productoptioncolor"]//div[@data-toggle="tooltip"]/@title')).replace(' +','')
    product_id = ''.join(tree.xpath('//input[@name="product_id"]/@value'))
    option_id = soup.find('label','colorboxbtn')
    if not option_id:
        colors = ''
        sizes = ''
        pass
    else:
        url = "https://donnalace.com/index.php?route=extension/optioncolorsize/optioncolorsize&product_id={}&product_option_value_id={}&option_id={}"
        r_p = s.get(url.format(product_id,option_id.get('rel'),option_id.get('data-value')))
        js = json.loads(r_p.content)
        # print(r_p.url)
        tree_p = html.fromstring(js['sizedata'],r_p.content)
        sizes = '| '.join(tree_p.xpath('//option//text()')[1:])
    row.writerow([
        r.url,
        sku,
        colors,
        sizes,
        category
    ])

inputs = pd.read_excel('product_links.xlsx', sheet_name='Sheet1')
for link in range(len(inputs)):
    try:
        crawl(inputs['product_url'].iloc[link], inputs['Category'].iloc[link])
    except:
        pass