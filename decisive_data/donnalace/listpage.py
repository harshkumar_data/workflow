from unicodedata import category
from requests import Session
from bs4 import BeautifulSoup as BS
from lxml import html
import csv

s = Session()
s.headers = {'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:96.0) Gecko/20100101 Firefox/96.01', 'Accept-Encoding': 'gzip, deflate', 'Accept': '*/*', 'Connection': 'keep-alive'}

f = open('product_links.csv','w',newline='',encoding='utf-8')
row = csv.writer(f)
row.writerow(['product_url','Category'])

def crawl_links(url):
    r = s.get(url)
    tree = html.fromstring(r.content)
    if not (tree.xpath('//div[@class="product-thumb"]')):
        pass
    else:
        category = '|'.join(tree.xpath('//ul[@class="breadcrumb"]//li//a//text()'))
        count = int(''.join(tree.xpath('//div[@class="col-sm-6 text-right page-result"]//text()')).split('(')[1].replace(' Pages)',''))
        for i in range(1,count+1):
            r_s = s.get(url+'?page={}'.format(i))
            print(r_s.url)
            tree = html.fromstring(r_s.content)
            p_links = tree.xpath('//div[@class="product-thumb"]//div[@class="image"]//a/@href')
            for link in p_links:
                row.writerow([link,category])

urls = open('cat_links.csv','r').read().split('\n')
for url in urls:
    crawl_links(url)