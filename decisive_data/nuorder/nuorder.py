from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import re
import json
import csv
import time
from requests import Session
from datetime import date
import pandas as pd
import mysql.connector
from lxml import html
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
import os
from loguru import logger
from gtts import gTTS
# from selenium.webdriver.common.action_chains import ActionChains

s = Session()

today = date.today()
d4 = today.strftime("%d-%m-%Y")
extension = '.csv'
# file_name = 'nuorder'+d4+extension
file_name = 'nuorder'+d4+extension
# folder_path = 'nuorder '+d4
folder_path = 'nuorder_ '+d4
if not os.path.exists(folder_path):
    os.makedirs(folder_path)

fi = open(os.path.join(folder_path,file_name),'w',newline='',encoding='utf-8')
row = csv.writer(fi)
row.writerow(['Category','URL','SKU','Title','Color','Size','STOCK','WholeSale_Price','Regular_Price'])

links = open('nu_link.txt','r',encoding='utf-8').read().split('\n')

open('co.txt','r+').truncate() #to clear the file content

path = 'C:/Users/HP/Downloads/geckodriver-v0.29.1-win64/geckodriver.exe'
driver = webdriver.Firefox(executable_path=path)
url = 'https://app.next.nuorder.com/brand/5d66b9eca107c756ab8af99a/gallery'
driver.get(url)
time.sleep(10)

#########################################################################################################################
#This code is for scrap data using json 

# out = open('nuorder_json','r',encoding='utf-8').read()
# js = json.loads(out)
# f = open('nuorder.csv','w',newline='',encoding='utf-8')
# row = csv.writer(f)
# for j in range(len(js)):
#     for i in range(len(js[j]['data'])):
#         item_id = js[j]['data'][i]['_id']
#         brand_id = js[j]['data'][i]['__brand']['__ref']
#         url = 'https://app.next.nuorder.com/brand/'+brand_id+'/gallery/item/'+item_id
#         sizes = js[j]['data'][i]['__sizes']
#         #s = []
#         #for si in range(len(sizes)):
#          #   size = sizes[si]['__size']
#         brands = js[j]['data'][i]['category']
#         sku = js[j]['data'][i]['style_number']
#         title = js[j]['data'][i]['name']
#         color = js[j]['data'][i]['color']
#         for si in range(len(sizes)):
#             size = sizes[si]['__size']
#         #ne.append(url)
#             row.writerow([url,sku,brands,title,color,size])
# f.close()
##########################################################################################################################
def login():
	user = driver.find_element_by_xpath('//input[@name="email"]')
	user.send_keys('kashifkiani77@hotmail.com')
	password = driver.find_element_by_xpath('//input[@name="password"]')
	password.send_keys('Donnalace123')
	submit = driver.find_element_by_xpath('//button[@type="submit"]')
	submit.click()

def main_code():
	# for item in range(0,len(click_item)):
		# click_item[item].click()
	for li in links:
		driver.get(li)
		time.sleep(20)
		# var wait = new WebDriverWait(Driver.Instance, TimeSpan.FromSeconds(10));
		# wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("processing")));
		# element_present = EC.presence_of_element_located((By.CLASS_NAME, 'panel'))
		# WebDriverWait(driver, timeout).until(element_present)
		# if(driver.find_element_by_class_name('panel')):
		tree = html.fromstring(driver.page_source)
		# item_url = driver.current_url
		# brand_id = re.search('brand/(.*?)/',li).group(1)
		# item_id = (li).split('/item/')[1]
		# m_url = 'https://app.next.nuorder.com/_a_p_i_/api/v3.0/item/{}/warehouse/5d66b9ec918b437926389ea4/replenishments?'.format(item_id)
		# # title = driver.find_element_by_class_name('fields').text
		title = ''.join(tree.xpath('//div[@class="fields"]//text()')).strip()
		# sku = driver.find_element_by_xpath('//div[@class="attributeList"]//div[@class="attribute"]//div[1][contains(string(),"Style Number")]//following::div[1]').text
		sku = ''.join(tree.xpath('//div[@class="attributeList"]//div[@class="attribute"]//div[1][contains(string(),"Style Number")]//following::div[1]//text()')).strip()
		category = ''.join(tree.xpath('//div[@class="attributeList"]//div[@class="attribute"]//div[1][contains(string(),"Category")]//following::div[1]//text()')).strip()
		# print(sku)
		# # colors = driver.find_element_by_xpath('//div[@class="colorLabel"]')
		colors = ''.join(tree.xpath('//div[@class="colorLabel"]//text()')).strip()
		# sizes = driver.find_elements_by_xpath('//div[@class="groupSizes"]//div[@class="sizeCell"]')
		sizes = tree.xpath('//div[@class="groupSizes"]//div[@class="sizeCell"]//text()')
		print(sizes)
		# sold_out =driver.find_elements_by_class_name('inventory')
		sold_out = tree.xpath('//div[@class="inventory"]')
		wholesale_price = ''.join(tree.xpath('//div[@class="wholesalePrice"]//text()')).strip()
		regular_price = ''.join(tree.xpath('//div[@class="msrp"]//text()')).strip().replace(' MSRP','')
		if(sold_out): 
			# file_path = 'E:/workflow/Decisive/nuorder/co.txt'
			if(os.stat('co.txt').st_size == 0):
				text = 'add cookie in the file'
				print(text)
				# language = 'en'
				# obj = gTTS(text=text, lang=language, slow=False)
				# obj.save('add_cookie.mp3')
				os.system('add_cookie.mp3')
				time.sleep(35)
			brand_id = re.search('brand/(.*?)/',li).group(1)
			item_id = (li).split('/item/')[1]
			m_url = 'https://app.next.nuorder.com/_a_p_i_/api/v3.0/item/{}/warehouse/5d66b9ec918b437926389ea4/replenishments?'.format(item_id)
			s.headers['Active-Brand']="{}".format(brand_id)
			cookie_file = open('co.txt','r').read()
			co = cookie_file
			print(co)
			s.headers={"Accept":"*/*",
			"Accept-Language":"en-US,en;q=0.5",
			"Active-Brand":"{}".format(brand_id),
			"Connection":"keep-alive",
			"Content-Type":"application/json",
			"Cookie":co.encode('utf-8'),
			"Host":"app.next.nuorder.com",
			"If-None-Match":'W/"2607-RgN049m7xPZ/LwB7V/fomKelVcY"',
			"Module":"base",
			"Referer":"driver.current_url",
			"TE":"Trailers",
			"User-Agent":"Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:89.0) Gecko/20100101 Firefox/89.0",
			"X-Requested-With":"XMLHttpRequest"}

			for si in range(len(sizes)):
			# print(len(sizes))
				r = s.get(m_url)
				# print(item_url)
				# print(item_id)
				# print(r.url)
				print(r.status_code)
				js = json.loads(r.content)
				# print(js)
				q = []
				for i in range(len(js[0]['replenishments'])):
					# print(i)
					q.append(js[0]['replenishments'][i]['quantity'])
				print('inserted')
				# print(len(q))
				for qty in range(len(q)):
					row.writerow([
						category,
						li,
						sku,
						title,
						colors,
						sizes[qty],
						q[qty],
						wholesale_price,
						regular_price
					])
			# else:
			# 	main_code()						
		else:
			for d in range(len(sizes)):
				print('Else inserted')
				row.writerow([
					category,
					li,
					sku,
					title,
					colors,
					sizes[d],
					'100',
					wholesale_price,
					regular_price
				])

			# close = driver.find_element_by_xpath('//button[@data-test="close"]').click()
			# close
		# fi.close()
		# else:
		# 	main_code()


# @logger.catch
def total_stock():
	con = mysql.connector.connect(host='donnalace.com',user ='neel_donnalace',passwd='KrishnaGod11##',database="neel_donnalace")
	mycon = con.cursor(buffered=True)
	# dup  = pd.read_csv(fi.name)
	# print(df)
	stock_sum = df.groupby(['SKU']).sum()
	indexs = stock_sum.index.to_list()
	for i in indexs:
		#print(i)
		# s_query = "select * from neel_donnalace.oc_product where sku='{}' and size_option_value_id={}".format(i[0],i[3])
		# #print(s_query)
		# mycon.execute(s_query)
		# #con.commit()
		# d = mycon.fetchone()
		# #print(d)
		# if(not d):
		#     insert_query = "insert into neel_donnalace.oc_product (sku,quantity,size_option_value_id) values('{}',{},{})".format(i[0],i[4],i[3])
		#     #print(insert_query)
		#     mycon.execute(insert_query)
		#     con.commit()
		#     print('inserted')
		# else:
		update_query = "update neel_donnalace.oc_product set quantity={} where sku='{}'".format(stock_sum.loc[i]["STOCK"],i)
		mycon.execute(update_query)
		con.commit()
		print(update_query)
		# print(update_query)
		print('Query Updated for Total Stock')
	# return df

def color_stock():
	con = mysql.connector.connect(host='donnalace.com',user ='neel_donnalace',passwd='KrishnaGod11##',database="neel_donnalace")
	mycon = con.cursor(buffered=True)
	df2 = pd.read_csv('color_id.csv')
	for a in range(len(df)):
		c = df.iloc[a]['Color']
		for i in range(len(df2)):
			name = df2.iloc[i]['name']
			ids = df2.iloc[i]['option_value_id']
			# if(c.lower().find(name.lower())!=-1):
			# 	# f.write(str(c)+'|'+str(ids)+'\n')
			# 	# update_query = "update neel_donnalace.oc_ebproduct_option_combination set quantity={} where sku='{}' and product_option_value_id={}".format(str(df.iloc[a]["STOCK"]),str(df.iloc[a]["SKU"]),str(ids))
			# 	update_query = "update neel_donnalace.oc_product_option_value set quantity={} where sku='{}' and option_value_id={}".format(str(df.iloc[a]["STOCK"]),str(df.iloc[a]["SKU"]),str(ids))
			# 	print(update_query)
			# 	mycon.execute(update_query)
			# 	con.commit()
			# 	print('query updated')
			# 	break
			if(c.lower() == name.lower()):
				# f.write(str(c)+'|'+str(ids)+'\n')
				# update_query = "update neel_donnalace.oc_ebproduct_option_combination set quantity={} where sku='{}' and product_option_value_id={}".format(str(df.iloc[a]["STOCK"]),str(df.iloc[a]["SKU"]),str(ids))
				update_query = "update neel_donnalace.oc_product_option_value set quantity={} where sku='{}' and option_value_id={}".format(str(df.iloc[a]["STOCK"]),str(df.iloc[a]["SKU"]),str(ids))
				print(update_query)
				mycon.execute(update_query)
				con.commit()
				print('Query Updated for Stock via Color')
				break

def size_Stock():
	con = mysql.connector.connect(host='donnalace.com',user ='neel_donnalace',passwd='KrishnaGod11##',database="neel_donnalace")
	mycon = con.cursor(buffered=True)
	df3 = pd.read_csv('size_id.csv')
	# for c in range(2052,len(df)):
	for c in range(2017,2052):
		print(c)
		cc = df.iloc[c]['Size'].replace('XS','Extra S').replace('2X LARGE','2Extra large').replace('S','Small').replace('M','Medium').replace('XXXL','3Extra large').replace('XXL','2Extra large').replace('XL','Extra L').replace('L','Large').replace('Extra large','XL').replace('XLarge','Extra Large')
		for d in range(len(df3)):
			name = df3.iloc[d]['name']
			ids = df3.iloc[d]['option_value_id']
			if(df.iloc[c]['Size'] == name):
				update_query = "update neel_donnalace.oc_ebproduct_option_combination set quantity={} where sku='{}' and  size_option_value_id='{}'".format(str(df.iloc[c]['STOCK']),str(df.iloc[c]['SKU']),str(ids))
				print(update_query)
				mycon.execute(update_query)
				con.commit()
				print('Query Updated for Total Stock')
			elif(cc == name):
				update_query = "update neel_donnalace.oc_ebproduct_option_combination set quantity={} where sku='{}' and  size_option_value_id='{}'".format(str(df.iloc[c]['STOCK']),str(df.iloc[c]['SKU']),str(ids))
				print(update_query)
				mycon.execute(update_query)
				con.commit()
				print('Query Updated for Stock via Size')

try:
	login()
	time.sleep(2)
except:
	login()
	time.sleep(2)

print('Crawl data from site')
main_code()
print('############# FINISHED #############')
# dup  = pd.read_csv(os.path.join(folder_path,file_name))
# # dup = pd.read_csv()
# df = dup.drop_duplicates()

# print('Update in Total Stock')
# total_stock()
# print('############# FINISHED #############')
# print('Update in Stock via Color Id')
# color_stock()
# print('############# FINISHED #############')
# print('Update in Stock via Size Id')
# size_Stock()
# print('############# FINISHED #############')

print('..............................End of Crawler...............................')


# size  = ['2X LARGE','S','XL','XS','XXL','XXXL','44DD','44F']






























# while True:
# 	try:
# 		time.sleep(3)
# 		print('try')
# 		click_item = driver.find_elements_by_class_name("item")
# 		# click_item = tree.xpath('//article[@class="item"]')
# 		print(len(click_item))
# 		main_code()	
# 	except:
# 		print('except')
# 		try:
# 			time.sleep(2)
# 			print('inside try')
# 			driver.execute_script("window.scrollBy(0, 2000);")
# 			time.sleep(5)
# 			click_item = driver.find_elements_by_class_name("item")
# 			# click_item = tree.xpath('//article[@class="item"]')
# 			time.sleep(3)
# 			main_code()
# 		except:
# 			time.sleep(3)
# 			print('inside except')
# 			driver.execute_script("window.scrollBy(0, 2000);")
# 			time.sleep(5)
# 			click_item = driver.find_elements_by_class_name("item")
# 			# click_item = tree.xpath('//article[@class="item"]')
# 			time.sleep(3)
# 			main_code()
# fi.close()	
# print('start updating in db')
# database_update()