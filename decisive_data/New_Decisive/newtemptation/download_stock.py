from selenium import webdriver
from selenium.webdriver.common.keys import Keys
# from selenium.webdriver.chrome.options import Options
import pandas as pd
import mysql.connector
from datetime import date
import os, openpyxl
from requests import Session
from lxml import html
import wget
import time

s = Session()
s.headers = {'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
 'Accept-Language': 'en-GB,en-US;q=0.9,en;q=0.8',
 'Connection': 'keep-alive',
 'Cookie': b'_ga=GA1.3.1684469793.1639826235; _gid=GA1.3.2117717066.1639826235; mage-cache-storage=%7B%7D; mage-cache-storage-section-invalidation=%7B%7D; mage-messages=; recently_viewed_product=%7B%7D; recently_viewed_product_previous=%7B%7D; recently_compared_product=%7B%7D; recently_compared_product_previous=%7B%7D; product_data_storage=%7B%7D; private_content_version=88dd38a7d0bc756a6d531a134007660b; PHPSESSID=bcf4d7204156f1afd5282a2ba1096c51; X-Magento-Vary=9bf9a599123e6402b85cde67144717a08b817412; form_key=xYekBVMpsKII6DcJ; mage-cache-sessid=true; section_data_ids=%7B%22customer%22%3A1639826434%2C%22compare-products%22%3A1639826434%2C%22last-ordered-items%22%3A1639826434%2C%22cart%22%3A1639826434%2C%22directory-data%22%3A1639826434%2C%22captcha%22%3A1639826434%2C%22instant-purchase%22%3A1639826434%2C%22persistent%22%3A1639826434%2C%22review%22%3A1639826434%2C%22wishlist%22%3A1639826434%2C%22recently_viewed_product%22%3A1639826434%2C%22recently_compared_product%22%3A1639826434%2C%22product_data_storage%22%3A1639826434%2C%22paypal-billing-agreement%22%3A1639826434%2C%22checkout-fields%22%3A1639826434%2C%22collection-point-result%22%3A1639826434%2C%22pickup-location-result%22%3A1639826434%7D',
 'Host': 'www.newtemptations.co.uk',
 'sec-ch-ua': 'Not A;Brand";v="99", "Chromium";v="96", "Google Chrome";v="96"',
 'sec-ch-ua-platform': 'Linux',
 'Sec-Fetch-Dest': 'document',
 'Upgrade-Insecure-Requests': '1',
 'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.93 Safari/537.36'}

def folder():
    today = date.today()
    d4 = today.strftime("%Y-%m-%d")
    folder_path = 'newtemptation '+d4
    if not os.path.exists(folder_path):
        os.makedirs(folder_path)
    return folder_path,d4

def open_script(driver):
    try:
        driver.execute_script("window.open('chrome-extension://oofgbpoabipfcfjapgnbbjjaenockbdp/popup.html')")
    except:
        open_script(driver)

folder_name,date_format = folder()
download_directory = f'/home/harsh/Documents/decisive_data/New_Decisive/newtemptation/{folder_name}/'

def resolve_captcha(driver):
    print('loading Login/Homepage')    
    open_script(driver)
    print("extesion")
    driver.switch_to.window(driver.window_handles[1])
    reg_btn_len=0
    blck_txt="No text"
    vpn_len=0
    while reg_btn_len==0:
        try:
            reg_btn_len = len(driver.find_elements_by_id('register-button'))
        except:
            reg_btn_len=0
        try:
            blck_txt = driver.find_element_by_id('main-content').text
        except:
            blck_txt="No text"
        if ("Requests to the server have been blocked by an extension" in blck_txt)==True:
            time.sleep(5)
            driver.get('chrome-extension://oofgbpoabipfcfjapgnbbjjaenockbdp/popup.html')
            blck_txt="No text"
        else:
            try:
                vpn_len = len(driver.find_elements_by_class_name('server-item__server__server-info__server-label'))
            except:
                vpn_len=0
            if vpn_len>0:
                break
    if vpn_len==0:
        pass_text = "PGXUWLCZZW"
        driver.find_elements_by_class_name('login-authcode-view__authcode-container__input')[0].send_keys(pass_text)
    while vpn_len==0:
        try:
            vpn_len = len(driver.find_elements_by_class_name('server-item__server__server-info__server-label'))
        except:
            vpn_len=0
    time.sleep(5) 
    for each in driver.find_elements_by_class_name('server-item__server__server-info__server-label'):
        if each.text=="Netherlands":
            each.click()
            break
    conn_text="No"
    while conn_text=="No":
        try:
            conn_text = driver.find_element_by_id("connected-disconnect-button").text
        except:
            conn_text="No"
    driver.switch_to.window(driver.window_handles[0])


def download_excel(driver):
    url = 'https://www.newtemptations.co.uk/live-stock-feed'
    driver.get(url)
    driver.implicitly_wait(20)
    driver.find_element_by_xpath('//div[@class="ldf-boxes clearfix"][1]//div[@class="ldf-box"][5]').click()
    # time.sleep(10)

def login_portal():
    path = '/home/harsh/Documents/decisive_data/New_Decisive/newtemptation/chromedriver'
    options = webdriver.ChromeOptions()
    # options.add_argument("--headless")
    # options.add_argument("--no-sandbox")
    prefs = {"download.default_directory": download_directory}
    options.add_experimental_option("prefs", prefs)
    options.add_argument("--user-data-dir=/home/harsh/.config/google-chrome")
    print('Start in few seconds')
    driver = webdriver.Chrome(options=options,executable_path=path)
    resolve_captcha(driver)
    url = 'https://www.newtemptations.co.uk/customer/account/login/'
    driver.get(url)
    # driver.implicitly_wait(15)
    time.sleep(5)
    if driver.current_url == url:
        user = driver.find_element_by_id('email').send_keys('raj2004141@hotmail.com')
        password = driver.find_element_by_id('pass').send_keys('Suhani99')
        login_submit = driver.find_element_by_id('send2').click()
    # driver.implicitly_wait(20)
    # url = 'https://www.newtemptations.co.uk/live-stock-feed'
    stock_url = 'https://www.newtemptations.co.uk/live-stock-feed'
    driver.get(stock_url)
    driver.implicitly_wait(20)
    driver.find_element_by_xpath('//div[@class="ldf-boxes clearfix"][1]//div[@class="ldf-box"][5]').click()


def stock_level_file():
    # url = 'https://www.newtemptations.co.uk/live-stock-feed'
    # r = s.get(url)
    # tree = html.fromstring(r.content)
    # stock_link = ''.join(tree.xpath('//div[@class="ldf-boxes clearfix"][1]//div[@class="ldf-box"][5]//a/@href'))
    # wget.download(stock_link,out=)

    stock_data = pd.read_csv(os.path.join(download_directory,f'stock_levels-{date_format}.csv'))
    all_data = pd.read_excel(download_directory.replace(f"{folder_name}/","")+'all_products.xlsx', engine="openpyxl")['SKU']
    df = pd.merge(stock_data,all_data).drop_duplicates()
    total_stock(df)
    size_stock(stock_data)

def total_stock(df):
    con = mysql.connector.connect(host='donnalace.com',user ='neel_donnalace',passwd='KrishnaGod11##',database="neel_donnalace")
    mycon = con.cursor(buffered=True)

    stock_sum = df.groupby(['SKU']).sum()
    kyes = stock_sum.index.to_list()
    for key in kyes:
        update_query = "update neel_donnalace.oc_product set quantity={} where sku='{}'".format(stock_sum.loc[key]["Stock"],key)
        mycon.execute(update_query)
        con.commit()
        print('query updated')

def size_stock(stock_data):
    con = mysql.connector.connect(host='donnalace.com',user ='neel_donnalace',passwd='KrishnaGod11##',database="neel_donnalace")
    mycon = con.cursor(buffered=True)
    for key in range(len(stock_data)):
        if int(stock_data.iloc[key]['Stock']) >=3:
            query = 'select * from neel_donnalace.oc_ebproduct_option_combination where sku="{}"'.format(str(stock_data.iloc[key]['SKU']))
            mycon.execute(query)
            data = mycon.fetchone()
            if(data):
                l.append(stock_data.iloc[key]['SKU'])
                update_query = "update neel_donnalace.oc_ebproduct_option_combination set quantity={} where sku='{}'".format(str(stock_data.iloc[key]['Stock']),str(stock_data.iloc[key]['SKU']))
                mycon.execute(update_query)
                con.commit()
                print('query updated size')
            else:
                print(str(stock_data.iloc[key]['SKU']))

l = []
# login_portal()
stock_level_file()
df = pd.DataFrame(l)
df.to_excel('newtemptaion_sku.xlsx')