from requests import Session
from lxml import html
import re
import csv
from datetime import date
import mysql.connector
import pandas as pd
import os
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.base import MIMEBase
from email import encoders
# from loguru import logger
import pandas as pd
import smtplib
import wget
import os
import zipfile
import time
from bs4 import BeautifulSoup as BS
import concurrent.futures

today = date.today()
d4 = today.strftime("%d-%m-%Y")
extension = '.xlsx'
file_name = 'alterego_utpal'+d4+extension
folder_path = 'alterego'+d4
subfolder = 'alterego_iamge'+d4
if not os.path.exists(folder_path):
    os.makedirs(os.path.join(folder_path, subfolder))

con = mysql.connector.connect(host='donnalace.com', user='neel_donnalace',
                              passwd='KrishnaGod11##', database="neel_donnalace")
mycon = con.cursor(buffered=True)

# f__ = open(os.path.join(folder_path, file_name), 'w', newline='', encoding='utf-8')
# row = csv.writer(f__)
# row.writerow(['Link', 'SKU', 'Color', 'Size', 'Stock Available','WholeSale_Price', 'Regular_Price'])

s = Session()
s.headers = {"Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
             "Accept-Language": "en-US,en;q=0.5",
             "Connection": "keep-alive",
             "Cookie": "_ga=GA1.2.874806791.1645254711; _hjSessionUser_2381822=eyJpZCI6IjAyZDc3MjBkLWE2YmMtNTM5Ni05ZmY5LTA3YjNiM2E4Mzg0OSIsImNyZWF0ZWQiOjE2NDUyNTQ3MTA5NzUsImV4aXN0aW5nIjp0cnVlfQ==; lwpcngStatus=2; PHPSESSID=ku3m7smej5uh1r2ip88g3qocda; wp_woocommerce_session_2a678c16a7ed2362e79a4918953e3486=1765%7C%7C1653463965%7C%7C1653460365%7C%7C27446406faa8ef6bd034565bbab4bfd1; _gid=GA1.2.52146985.1653291169; mailchimp_landing_site=https%3A%2F%2Falterego-lingerie.com%2F%3Fwc-ajax%3Dget_refreshed_fragments; _gat=1; _hjIncludedInP…6IjE5OGQ3ZTQ0LTQ4MmMtNGM3OC1hYzc0LTQ1MmJkNDU1Nzg4YiIsImNyZWF0ZWQiOjE2NTMyOTExNjk0NzMsImluU2FtcGxlIjp0cnVlfQ==; _hjAbsoluteSessionInProgress=1; __stripe_mid=8cf3eeee-1b0c-4e2b-9ba2-1a7c0147227c11181b; __stripe_sid=19c586a5-8a55-412c-99b0-76d049abdd2fc072af; _lscache_vary=a5b8bd8c058b164f9d8f79e1473911a5; wordpress_logged_in_2a678c16a7ed2362e79a4918953e3486=supplychain%40donnalace.com%7C1653463978%7CiYfs8E1aOFewDw3l0p5m9KElo5ZqF8p3aENtOcLcMqU%7Cef5af94e3ba3ed986b9dcb3089a17715023fa8ac67456245ae230dad7e46ac91".encode('utf-8'),
             "Host": "alterego-lingerie.com",
             "Sec-Fetch-Dest": "document",
             "Sec-Fetch-Mode": "navigate",
             "Sec-Fetch-Site": "none",
             "Sec-Fetch-User": "?1",
             "Upgrade-Insecure-Requests": "1",
             "User-Agent": "Mozilla/5.0 (X11; Linux x86_64; rv:100.0) Gecko/20100101 Firefox/100.0"}


data = []


def link_crawl(url):
    r = s.get(url)
    print(r.url)
    tree = html.fromstring(r.content)
    soup = BS(r.content, 'html.parser')
    # brand = soup.find('h1','woocommerce-products-header__title page-title').text
    links = tree.xpath(
        '//a[@class="card card-link h-100 border-0 bg-light shadow-sm text-dark no-decoration d-flex text-center"]/@href')
    print(links)
    for link in links:
        # data.append({"Links":link,"brand":brand})
        data.append({"Links": link})
    try:
        next_page = soup.find('ul', 'page-numbers').find('span',
                                                         'page-numbers current').previous.findNextSibling()
        if(next_page):
            next_url = next_page.find('a').get('href')
            link_crawl(next_url)
    except:
        pass


# l = []
def data_scrap(url):
    r_s = s.get(url[0])
    if(r_s.status_code != 200):
        pass
    else:
        print('start', r_s.url, r_s.status_code)
        tree_s = html.fromstring(r_s.content)
        sku = re.search('"sku":"(.*?)"',
                        r_s.text).group(1).replace('\/', '/').replace("'", "")
        stock = re.findall('name="stock_quantity" value="(.*?)" />', r_s.text)
        trs = tree_s.xpath(
            '//table[@class="table table-striped table-bordered"]//tbody//tr')
        for tr in trs:
            color = tr.xpath(
                '//span[@class="attr attr-attribute_pa_colour"]//text()')
            sizes = tr.xpath(
                '//span[@class="attr attr-attribute_pa_size"]//text()')
        cust_price = ''.join(tree_s.xpath(
            '//div[@class="mt-2 mb-4"]//div[contains(string(),"RRP:")]//following-sibling::span//text()')).strip().replace('£', '')

        if(tree_s.xpath('//div[@class="h3 serif-font"]//ins//span[@class="woocommerce-Price-amount amount"]')):
            wholesale_price = ''.join(list(set(tree_s.xpath(
                '//div[@class="h3 serif-font"]//ins//span[@class="woocommerce-Price-amount amount"]//text()')))).strip().replace('£', '')
        else:
            wholesale_price = ''.join(tree_s.xpath(
                '//div[@class="h3 serif-font"]//span[@class="woocommerce-Price-amount amount"]//text()')).strip().replace('£', '')

        for detail in zip(color, sizes, stock):
            print('enter')
            values = [r_s.url, sku, ''.join(detail[0]).strip(), ''.join(detail[1]).strip(), ''.join(detail[2]).strip(), wholesale_price, cust_price]
            row.writerow(values)
            print(values)

## Update Discounted price
def discount_price(product_id,discounted_price):
    update_query = "update neel_donnalace.oc_product_special set price={} , date_modified = {} where product_id='{}'".format(discounted_price, str(today.strftime("%Y-%m-%d")), product_id)
    mycon.execute(update_query)
    con.commit()
    print(update_query)
    print('discount price updated')

def update_price(df):
    df = df.drop_duplicates('SKU')
    for i in range(921, len(df)):
        update_query = "update neel_donnalace.oc_product set ean = {} , price = {} , date_modified ={} where sku='{}'".format(df.iloc[i]["WholeSale_Price"], df.iloc[i]["Regular_Price"], str(today.strftime("%Y-%m-%d")), str(df.iloc[i]["SKU"]))
        mycon.execute(update_query)
        con.commit()
        print('Price updated')
        discount_price(df.iloc[i]["product_id"], df.iloc[i]["discounted_price"])

# Update in Total Stock
def total_stock():
    con = mysql.connector.connect(
        host='donnalace.com', user='neel_donnalace', passwd='KrishnaGod11##', database="neel_donnalace")
    mycon = con.cursor(buffered=True)

    stock_sum = df.groupby(['SKU']).sum()
    indexs = stock_sum.index.to_list()
    for i in indexs:
        print(i)
        update_query = "update neel_donnalace.oc_product set quantity={} , date_modified ='{}' where sku='{}'".format(
            int(stock_sum.loc[i]["Stock Available"]), str(today.strftime("%Y-%m-%d")), str(i))
        mycon.execute(update_query)
        con.commit()
        print('query updated')

# # Stock Update via Color Id


def color_stock():
    con = mysql.connector.connect(
        host='donnalace.com', user='neel_donnalace', passwd='KrishnaGod11##', database="neel_donnalace")
    mycon = con.cursor(buffered=True)

    df2 = pd.read_csv('color_id.csv')
    for a in range(len(df)):
        print(a)
        c = df.iloc[a]['Color']
        cc = c.split('/')[0]
        for i in range(len(df2)):
            name = df2.iloc[i]['name']
            ids = df2.iloc[i]['option_value_id']
            if(name == cc):
                print('if')
                # f.write(str(al.iloc[a]['SKU'])+'|'+str(cc)+'|'+str(ids)+'\n')
                update_query = "update neel_donnalace.oc_product_option_value set quantity={} where sku='{}' and option_value_id={}".format(
                    str(df.iloc[a]["Stock Available"]), str(df.iloc[a]["SKU"]), str(ids))
                print(update_query)
                mycon.execute(update_query)
                con.commit()
                print('query updated')
                break
            elif(re.search('\((.*?)\)', c)):
                s_space = c.split(' ')[0]
                # print(s_space)
                b_color = re.search('\((.*?)\)', c).group(1)
                # print(b_color)
                if(name == b_color):
                    print('......')
                    # f.write(str(df.iloc[a]['SKU'])+'|'+str(b_color)+'|'+str(ids)+'\n')
                    update_query = "update neel_donnalace.oc_product_option_value set quantity={} where sku='{}' and option_value_id={}".format(
                        str(df.iloc[a]["Stock Available"]), str(df.iloc[a]["SKU"]), str(ids))
                    print(update_query)
                    mycon.execute(update_query)
                    con.commit()
                    print('query updated')
                    break
                elif(name == s_space):
                    print('cccccccccccccccccccc')
                    # f.write(str(df.iloc[a]['SKU'])+'|'+str(s_space)+'|'+str(ids)+'\n')
                    update_query = "update neel_donnalace.oc_product_option_value set quantity={} where sku='{}' and option_value_id={}".format(
                        str(df.iloc[a]["Stock Available"]), str(df.iloc[a]["SKU"]), str(ids))
                    print(update_query)
                    mycon.execute(update_query)
                    con.commit()
                    print('query updated')
                    break
                elif(name.find(s_space) != -1):
                    print('sssssssssssssssss')
                    # f.write(str(df.iloc[a]['SKU'])+'|'+str(s_space)+'|'+str(ids)+'\n')
                    update_query = "update neel_donnalace.oc_product_option_value set quantity={} where sku='{}' and option_value_id={}".format(
                        str(df.iloc[a]["Stock Available"]), str(df.iloc[a]["SKU"]), str(ids))
                    print(update_query)
                    mycon.execute(update_query)
                    con.commit()
                    print('query updated')
                    break
                elif(name.find(b_color) != -1):
                    # f.write(str(df.iloc[a]['SKU'])+'|'+str(b_color)+'|'+str(ids)+'\n')
                    update_query = "update neel_donnalace.oc_product_option_value set quantity={} where sku='{}' and option_value_id={}".format(
                        str(df.iloc[a]["Stock Available"]), str(df.iloc[a]["SKU"]), str(ids))
                    print(update_query)
                    mycon.execute(update_query)
                    con.commit()
                    print('query updated')
                    break
                else:
                    print('ffffffffffffffffffff')
                    update_query = "update neel_donnalace.oc_product_option_value set quantity={} where sku='{}' and option_value_id={}".format(
                        str(df.iloc[a]["Stock Available"]), str(df.iloc[a]["SKU"]), str(ids))
                    print(update_query)
                    mycon.execute(update_query)
                    con.commit()
                    print('query updated')
                    break
            elif(name == c):
                # f.write(str(df.iloc[a]['SKU'])+'|'+str(c)+'|'+str(ids)+'\n')
                update_query = "update neel_donnalace.oc_product_option_value set quantity={} where sku='{}' and option_value_id={}".format(
                    str(df.iloc[a]["Stock Available"]), str(df.iloc[a]["SKU"]), str(ids))
                print(update_query)
                mycon.execute(update_query)
                con.commit()
                print('query updated')
                break

# Stock Update via Size Id

    
def size_stock(df):
    today = date.today()
    d4 = today.strftime("%d-%m-%Y")
    extension = '.xlsx'
    size_file = 'alter_size'+d4+extension
    # f_out = open(os.path.join(folder_path,size_file),'w',encoding='utf-8',newline='')
    # s_row = csv.writer(f_out)
    # s_row.writerow(['SKU','Size_Name','size_id','Stock Available'])
    # df3 = pd.read_csv('size_id.csv')

    # for c in range(1800, len(df)):
    #     if type(df.iloc[c]['Size']) == int:
    #         pass
    #     else:
    #         cc = df.iloc[c]['Size'].replace('Size ','').replace('XS','Extra S').replace('S','Small').replace('M','Medium').replace('XXL','2Extra large').replace('XL','Extra L').replace('L','Large').replace('Extra large','XL').replace('XLarge','Extra Large').split('/')
    #         new = df.iloc[c]['Size'].replace('Size ','').replace('XS','Extra S').replace('S','Small').replace('M','Medium').replace('XXL','2Extra large').replace('XL','Extra L').replace('Extra large','XL').replace('XLarge','Extra Large').replace('L','Large')
    #         for cc_m in cc:
    #             for d in range(len(df3)):
    #                 name = df3.iloc[d]['name']
    #                 ids = df3.iloc[d]['option_value_id']
    #                 if(df.iloc[c]['Size'] == name):
    #                     s_row.writerow([str(df.iloc[c]['SKU']),str(name),str(ids),str(df.iloc[c]['Stock Available'])])
    #                     break
    #                 elif(cc_m == name):
    #                     s_row.writerow([str(df.iloc[c]['SKU']),str(name),str(ids),str(df.iloc[c]['Stock Available'])])
    #                     break
    #                 elif(re.search('\((.*?)\)',new)):
    #                     n = re.search('\((.*?)\)',new).group(1).split('/')
    #                     for n_m in n:
    #                         if(n_m == name):
    #                             s_row.writerow([str(df.iloc[c]['SKU']),str(name),str(ids),str(df.iloc[c]['Stock Available'])])
    #                             break
    #                 elif(re.search('\((.*?)\)',df.iloc[c]['Size'])):
    #                     ss = re.search('\((.*?)\)',c).group(1).replace('(','').replace(')','').replace('XS','Extra S').replace('S','Small').replace('M','Medium').replace('XXL','2Extra large').replace('XL','Extra L').replace('L','Large').replace('Extra large','XL').replace('XLarge','Extra Large')
    #                     if(ss == name):
    #                         s_row.writerow([str(df.iloc[c]['SKU']),str(name),str(ids),str(df.iloc[c]['Stock Available'])])
    #                         break
    # f_out.close()

    df_dup = pd.read_excel(os.path.join(folder_path, size_file))
    print(df_dup)

    con = mysql.connector.connect(
        host='donnalace.com', user='neel_donnalace', passwd='KrishnaGod11##', database="neel_donnalace")
    mycon = con.cursor(buffered=True)

    for i in range(998, len(df_dup)):
        if int(df_dup.iloc[i]['Stock Available']) >= 3:
            query = "select sku from neel_donnalace.oc_ebproduct_option_combination where sku='{}'".format(
                str(df_dup.iloc[i]['SKU']))
            mycon.execute(query)
            data = mycon.fetchone()
            if(data):
                update_query = "update neel_donnalace.oc_ebproduct_option_combination set quantity={},DATE='{}' where sku='{}' and  size_option_value_id='{}'".format(
                    str(df_dup.iloc[i]['Stock Available']), today.strftime("%Y-%m-%d"), str(df_dup.iloc[i]['SKU']), str(df_dup.iloc[i]['size_id']))
                print(update_query)
                mycon.execute(update_query)
                con.commit()
                print('query updated size')
            else:
                try:
                    print(df_dup.iloc[i]['SKU'])
                except:
                    pass




# urls = ['https://alterego-lingerie.com/brands/zohara/','https://alterego-lingerie.com/brands/roza/','https://alterego-lingerie.com/brands/ballerina/','https://alterego-lingerie.com/brands/beauty-night/','https://alterego-lingerie.com/brands/confidante/','https://alterego-lingerie.com/brands/control-body/','https://alterego-lingerie.com/brands/corin/','https://alterego-lingerie.com/brands/gracya/','https://alterego-lingerie.com/brands/shirley-of-hollywood/','https://alterego-lingerie.com/brands/wolbar/','https://alterego-lingerie.com/brands/yesx/','https://alterego-lingerie.com/brands/gabriella/','https://alterego-lingerie.com/brands/irall/','https://alterego-lingerie.com/brands/irall-erotic/','https://alterego-lingerie.com/brands/me-seduce/','https://alterego-lingerie.com/brands/provocative/']
# for url in urls:
#     link_crawl(url)
# df = pd.DataFrame(data)
# df.to_excel('alter_links_new_arrivals.xlsx')


## Start Code from here ###
# df = pd.read_excel('alter_links.xlsx', engine='openpyxl')
# print(df)

# with concurrent.futures.ThreadPoolExecutor(max_workers=30) as executor:
#     list(executor.map(data_scrap, df.values))
# f__.close()

dup = pd.read_excel(os.path.join(folder_path, file_name))
df = dup.drop_duplicates()
print(df)

# print('Update in Total Stock')
# total_stock()
# print('############# FINISHED #############')

print('Update in Stock via Size Id')
try:
    size_stock(df)
except:
    con = mysql.connector.connect(
        host='donnalace.com', user='neel_donnalace', passwd='KrishnaGod11##', database="neel_donnalace")
    mycon = con.cursor(buffered=True)
    size_stock(df)

# print('Update in Price')
# update_price(df)

print('############# FINISHED #############')


print('......................End of Crawler...........................')
