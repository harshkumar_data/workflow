from requests import Session
from lxml import html
import json
from bs4 import BeautifulSoup as BS
import csv

f = open('linga_links.csv','w',newline='',encoding='utf-8')
# row = csv.writer(f)

s = Session()
co = "_ga=GA1.2.883753888.1622793754; userpref_language=en; _gid=GA1.2.200056545.1623314202; lastseen_stock=%5B%221400BD%22%5D; ci_session=VTdcYgNkUG5Xe1d0AWhQNVBmVm4DIwVwATUGdgZ2BW0HOFQ%2FU18FMl45AyUHb1cgAWwCYVMyAG4DJ1A3V2ZQYltoBDUDNlNjADJTYwVjAzxVZlxrAzFQY1cwVzMBZ1BhUGFWYgMyBTUBYgYxBjcFNAdvVGBTMAU4XmoDJQdvVyABbAJjUzAAbgMnUDtXJVAJW2sEYwNkUyAANVMlBSUDf1VtXCsDalBlVzNXPQFwUDVQZlZnAy8FMQFhBjQGKwU1B2NUf1M1BWNefwM8BydXaQFnAmJTOgB2A3BQIVcwUCRbVQRmA2dTNwA%2BUyIFdANmVSVcYgNnUGxXOlclAR9Qa1AsVj0DbQVvATEGKgYwBSoHZ1RxUygFDF40A2kHMFc8ASECIVMgABoDUVByV2RQZlskBDcDO1NyAAdTPwU4A2tVYlxjA3BQLFc2VzMBaVAkUCRWIgM7BTsBaQYrBjUFLQd3VBZTZQU4XjYDaAd7V2EBZgJjUzAAZAM0UGJXZFB2W0wEbgNyUzcANlM5BS4DclVuXGEDflBkVyJXPAEhUD5QZ1ZnAzsFIQE8BmQGdgVwBwhUMFNjBS9eNANxBz1XJwEvAnBTOwA9Az9QY1djUGRbOQQ0AzJTZgBmU28FZgNmVSs%3D"
s.headers ={"Accept":"application/json, text/javascript, */*; q=0.01",
"Accept-Language":"en-US,en;q=0.5",
"Connection":"keep-alive",
"Content-Length":"18",
"Content-Type":"application/x-www-form-urlencoded; charset=UTF-8",
"Cookie":co.encode('utf-8'),
"Host":"b2b.lingadore.com",
"Origin":"https://b2b.lingadore.com",
"Referer":"https://b2b.lingadore.com/catalog",
"TE":"Trailers",
"User-Agent":"Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:88.0) Gecko/20100101 Firefox/88.0",
"X-Requested-With":"XMLHttpRequest",}

l = []
for i in range(1,43):
    data = {"offset": "{}".format(12*i),"limit": "12"}
    url = 'https://b2b.lingadore.com/catalog/ajax_get_items'
    r = s.post(url,data=data)
    #print(data)
    #print(r.content)
    js = json.loads(r.content)
    #print(js)
    raw = js['html']
    tree = html.fromstring(raw)
    links = tree.xpath('//div[@class="item"]/@data-detail')
    for li in links:
        f.write(li+'\n')
    print(i)
    # print(r.status_code)
    # print(links)