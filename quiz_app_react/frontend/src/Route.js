import { BrowserRouter, Routes, Route } from "react-router-dom"
import Standards from "./Pages/standard"
import Subjects from "./Pages/subjects"
import Questions from "./Pages/detailpage"

const MyRoutes = () => {
    return (
        <BrowserRouter>
            <Routes>
                <Route path='/' element={<Standards />} />
                <Route path='/:standard' element={<Subjects />} />
                <Route path="/:standard/:subject/" element={<Questions />}/>
            </Routes>
        </BrowserRouter>
    )
}

export default MyRoutes