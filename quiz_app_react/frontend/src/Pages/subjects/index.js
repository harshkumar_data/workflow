import { Link, useParams } from "react-router-dom"
import axios from "axios";
import { useEffect, useState } from "react";
import TitleCase from "../../components/case_converter";
import pagetitle from "../../components/page_title";

export default function Subjects() {
    let params = useParams();
    const new_params = params.standard.split('-')[6]

    const [subjects, SetSubjects] = useState([]);

    
    useEffect(() => {
        axios.get(`http://127.0.0.1:5000/${new_params}`).then(response => {
            SetSubjects(response?.data)
        }).catch(error => {
            console.log(error)
        })
    }, [])
    pagetitle(`${new_params} | Subjects`)

    return (
        <div className="container align-text-center pt-4 mt-4">
            <div className="heading text-center mb-5">
                <h1>{TitleCase(params?.standard)}</h1>
            </div>
            <br></br>
            <div className="row g-4 bg-2 mt-6">
                {
                    subjects?.map((item, index) => {
                        let title = item.subject
                        return (
                            <div className="col col-lg-4" key={index}>
                                <div className="p-3 border bg-light c-black text-center h-100 w-auto fs-3 fw-light">
                                    <Link to={title} id={title} className="text-decoration-none text-info">
                                        {title}
                                    </Link>
                                </div>
                            </div>
                        )
                    })
                }
            </div>
        </div>
    )
}