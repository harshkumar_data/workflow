import axios from "axios"
import { useEffect, useState, useRef } from "react";
import { useParams } from "react-router-dom"
import pagetitle from "../../components/page_title";

export default function Questions() {
    let params = useParams();
    let subject = params.subject.replace(/-/g, '/')
    let standard = params.standard.split('-')[6]
    let title = `Class ${standard} of ${subject}`
    const [questions, SetQuestions] = useState(null);
    let recordsPerPage = 10;
    let indexofFirstRecord = 10;

    useEffect(() => {
        axios.get(`http://127.0.0.1:5000/${standard}/${subject}/?limit=${recordsPerPage}&offset=${indexofFirstRecord}`).then(response => {
            SetQuestions(response?.data)
            pagetitle(title)
        }).catch(error => {
            console.log(error)
        })
    }, [])
    return (
            <>
            <div className="container mt-4">
                <div className="heading text-center m-4">
                    <h1>{title}</h1>
                </div>
                <div className="row g-4 bg-2">{
                    questions?.map((item, index) => {
                        return (
                            <div className="quiz" key={index}>
                                <div className="question">
                                    {item.question}
                                </div>
                                <div className="answer d-flex mt-2">
                                    <div className="">
                                        Answer :
                                    </div>
                                    <div className="ms-2">
                                        {item.answer}
                                    </div>
                                </div>
                            </div>
                        )
                    })
                }
                </div>
            </div>
            </>
            )
}