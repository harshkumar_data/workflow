import requests
import re
from bs4 import BeautifulSoup as BS
import datatbase_managerV2

db_client = datatbase_managerV2.get_client()
collection = db_client['seaching_dataV2']


def get_headers():
    cookies = {
    'session-id': '260-1476310-7223402',
    'session-id-time': '2082787201l',
    'csm-hit': 'S8X5P163XEGMC4JKRY51+s-JC2N6C4TAH66XVDH1VJF|1676212405154',
    'ubid-acbin': '259-4672526-8427407',
    'sst-acbin': 'Sst1|PQFuzFDz4GnxDQTndGtli-9VB_tt5vu4tPWRMIO_fvazvjuM7Iu_vw5_OTi9ngdbHh2tEB7EZKZR7AM6RmpLSt002_0SbUuo84B3pp2T3jpcsrnJBrfprTZHlAYnwGTmT3xdPi9_TOZGuM-nMjHo703f3lz_H4vmTc-YO9Hwn3CJ8K7KegSV3HZrn7u5aWNgM4mnEoU85Ll5_PGTh0bUDaIFXfu5BF3A1kxSMvYQcm3e68opuYQLuT3Hl_k1aJlehHqN',
    'session-token': 'nLnUbwBsuY+cw433Muk/oCPymGbJ/Fg9lb4MXwwQhvQF/TyiKo5Yk5bfnEyDRr/aNarQ8Bh5I9abok2EfMSMXAQH9kPPMNxWDuFudG0AwwGxoTHstO9srZlWx6haCtFxBSRSB3OmRwhPMV5jBhxWcxAf9E6Fx/iKtDb6mOjDlzOf3O8I3SK0XiS17zfgz0JJmK2t+DBdKkX+mR2e2DU2DkprRdibfSnUpZsBHiOPcj0=',
    'i18n-prefs': 'INR',
    'urtk': '%7B%22at%22%3A%22Atna%7CEwICIJcwXUIabFVLDxLMzgxw9HaJMZb5pcFdNcA_oK61QnsWFVOStvd6fo0jn2GfTuHl_7cgKqU769iQJDmpWn9hpMFhZEe3GMDGW9jnjfxUHRHj94jhyCBATDtyQ1pBvIhVPTlrXzFf8En7cm7-Ig1hn6MaZ38yIXyuFqCoSKKPt7D_OW-0IAo2gEde6qL4-2QdaaA4HQCuIr5CCgXwFSu-JXFwrcznRMP739X-kZl93y4K-G6C68uxhOaZ7-7u6jc2s4uAnlhS84mCI6Zx-dIXoSay4-AHW-qQbjTgMc3zPTXv9Cd7Frb2HmHlbIhyEA4GZ2A%22%2C%22atTtl%22%3A1676205569849%2C%22rt%22%3A%22Atnr%7CEwICIBcKbSj0UDtOPcxZRBJ4kIHKrei1BHQoWToGp-36yGx27VemiJOeLnyvsW7bZ3CuUps8F-c50yaDtX4jW-JeoJ9BreQGemkrBQP6lOeCKPBh0hJzHlr3kPgSqy6kntUbfEX6S8jk1JMLvpY-Y3fRkgaGoDyfJjCft0eO7KqFpTvtcR8kIG7SOpi8R689O8TUz8Yc3ufm7MViQNaIGDBO-zxhszv0vXzTGWDEqu0LVgBMHQG5e8kFKUmHH5rj_dMmfCRuW0e0EpAtrnft3ak62m-xbEKgk5KcrRxtesrQPMc_Y4Pfa0EK2QN6M5UIaIeKGxFHuhLN9LzkN00K2Lrh71Tw%22%7D',
    }

    headers = {
        'User-Agent': 'Mozilla/5.0 (iPhone; CPU iPhone OS 14_6 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/14.0.3 Mobile/15E148 Safari/604.1',
        'Accept': 'text/html,*/*',
        'Accept-Language': 'en-US,en;q=0.5',
        # 'Accept-Encoding': 'gzip, deflate, br',
        'X-Requested-With': 'XMLHttpRequest',
        'Content-Type': 'application/json',
        'X-Amazon-s-swrs-version': '3EC77192C9F4FD1ED5E2E7965D256328,D41D8CD98F00B204E9800998ECF8427E',
        'X-Amazon-s-mismatch-behavior': 'FALLBACK',
        'X-Amazon-s-fallback-url': 'https://www.amazon.in/s?k=laptop&page=1',
        'x-amazon-rush-fingerprints': 'AmazonRushAssetLoader:1CA6A106741C59A91402D2CDBDD672C5D8FF4A29|AmazonRushFramework:E15C860B97000BD31B1AF38E2CF90432B3A093CB|AmazonRushRouter:D3A3E10610122E482D37F7EC7973CC360E093BB1',
        'Origin': 'https://www.amazon.in',
        'Connection': 'keep-alive',
        'Referer': 'https://www.amazon.in/s?k=laptop&ref=nb_sb_noss',
        # 'Cookie': 'session-id=260-1476310-7223402; session-id-time=2082787201l; csm-hit=S8X5P163XEGMC4JKRY51+s-JC2N6C4TAH66XVDH1VJF|1676212405154; ubid-acbin=259-4672526-8427407; sst-acbin=Sst1|PQFuzFDz4GnxDQTndGtli-9VB_tt5vu4tPWRMIO_fvazvjuM7Iu_vw5_OTi9ngdbHh2tEB7EZKZR7AM6RmpLSt002_0SbUuo84B3pp2T3jpcsrnJBrfprTZHlAYnwGTmT3xdPi9_TOZGuM-nMjHo703f3lz_H4vmTc-YO9Hwn3CJ8K7KegSV3HZrn7u5aWNgM4mnEoU85Ll5_PGTh0bUDaIFXfu5BF3A1kxSMvYQcm3e68opuYQLuT3Hl_k1aJlehHqN; session-token=nLnUbwBsuY+cw433Muk/oCPymGbJ/Fg9lb4MXwwQhvQF/TyiKo5Yk5bfnEyDRr/aNarQ8Bh5I9abok2EfMSMXAQH9kPPMNxWDuFudG0AwwGxoTHstO9srZlWx6haCtFxBSRSB3OmRwhPMV5jBhxWcxAf9E6Fx/iKtDb6mOjDlzOf3O8I3SK0XiS17zfgz0JJmK2t+DBdKkX+mR2e2DU2DkprRdibfSnUpZsBHiOPcj0=; i18n-prefs=INR; urtk=%7B%22at%22%3A%22Atna%7CEwICIJcwXUIabFVLDxLMzgxw9HaJMZb5pcFdNcA_oK61QnsWFVOStvd6fo0jn2GfTuHl_7cgKqU769iQJDmpWn9hpMFhZEe3GMDGW9jnjfxUHRHj94jhyCBATDtyQ1pBvIhVPTlrXzFf8En7cm7-Ig1hn6MaZ38yIXyuFqCoSKKPt7D_OW-0IAo2gEde6qL4-2QdaaA4HQCuIr5CCgXwFSu-JXFwrcznRMP739X-kZl93y4K-G6C68uxhOaZ7-7u6jc2s4uAnlhS84mCI6Zx-dIXoSay4-AHW-qQbjTgMc3zPTXv9Cd7Frb2HmHlbIhyEA4GZ2A%22%2C%22atTtl%22%3A1676205569849%2C%22rt%22%3A%22Atnr%7CEwICIBcKbSj0UDtOPcxZRBJ4kIHKrei1BHQoWToGp-36yGx27VemiJOeLnyvsW7bZ3CuUps8F-c50yaDtX4jW-JeoJ9BreQGemkrBQP6lOeCKPBh0hJzHlr3kPgSqy6kntUbfEX6S8jk1JMLvpY-Y3fRkgaGoDyfJjCft0eO7KqFpTvtcR8kIG7SOpi8R689O8TUz8Yc3ufm7MViQNaIGDBO-zxhszv0vXzTGWDEqu0LVgBMHQG5e8kFKUmHH5rj_dMmfCRuW0e0EpAtrnft3ak62m-xbEKgk5KcrRxtesrQPMc_Y4Pfa0EK2QN6M5UIaIeKGxFHuhLN9LzkN00K2Lrh71Tw%22%7D',
        'Sec-Fetch-Dest': 'empty',
        'Sec-Fetch-Mode': 'cors',
        'Sec-Fetch-Site': 'same-origin',
        # Requests doesn't support trailers
        # 'TE': 'trailers',
    }

    return cookies, headers

def get_pageload():
    cookies, headers = get_headers()
    params = {
        'fs': 'true',
        'i': 'computers',
        'page': '8',
        'qid': '1676198345',
        'ref': 'is_pn_7',
        'rh': 'n:22963797031',
    }
    data = {
        'page-content-type': 'atf',
        'prefetch-type': 'rq',
        'customer-action': 'pagination',
    }

    response = requests.post('https://www.amazon.in/s/query', params = params, cookies = cookies, headers = headers, json = data)
    response = response.text.replace('\\', '')
    page_load = re.search('"payload" : "(.*?)"', response).group(1)
    reqeuest_id = re.search('"requestId" : "(.*?)"', response).group(1)
    return page_load, reqeuest_id


def crawl_listpage():
    # page_load, reqeuest_id = get_pageload()
    cookies, headers = get_headers()
    # params = {
    #     'i': 'computers',
    #     'page': '1',
    #     'rh': 'n:22963797031',
    # }
    params = {
        "k": "laptop",
        "page": "1"
    }

    data = {
        'progressiveScroll': True,
        'wIndexMainSlot': 0,
        'customer-action': 'pagination',
    }


    response = requests.post('https://www.amazon.in/s/query', params = params, cookies = cookies, headers = headers, json = data)
    response = response.text.replace('\\', '')
    raw_asin_ids = re.findall('"asin" : "(.*?)"',response)
    # asin_ids = []
    for asin_id in raw_asin_ids:
        if asin_id:
            # asin_ids.append({"_id": asin_id, "url": f'https://www.amazon.in/dp/{asin_id}'})
            try:
                collection.insert_one({"_id": asin_id, "url": f'https://www.amazon.in/dp/{asin_id}'})
            except:
                pass

crawl_listpage()

