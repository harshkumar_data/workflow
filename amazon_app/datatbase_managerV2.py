from pymongo import MongoClient


def get_client():
    uri = 'mongodb://localhost:27017'
    return MongoClient(uri)['amazon_app']


def get_records(collection, search_query):
    raw = collection.find(search_query)
    return [record for record in raw]
