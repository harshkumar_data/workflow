import requests
import pandas
from lxml import html

headers = {
    'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:107.0) Gecko/20100101 Firefox/107.0'}


def crawl_detailpage(product_url):
    response = requests.get(product_url, headers=headers)
    tree = html.fromstring(response.text)
    description = ' '.join(tree.xpath(
        '//div[@class="product-description"]//ul//li//text()'))
    return description

def crawl_listpage(category_url):
    response = requests.get(category_url)
    json_data = response.json()
    products = json_data.get("products")
    category = json_data.get("categoryName")
    for product in products:
        product_url = 'https://www.asos.com/fr/' + product.get("url")
        data = {
            "Collection": "Femme",
            "Category": category,
            "Product_URL": product_url,
            "Brand": product.get("brandName"),
            "Color": product.get("colour"),
            "Product Code": product.get("productCode"),
            "Current Price": product.get('price').get('current').get('text') if product.get('price') else '',
            "Designation": product.get("name"),
            "Description": crawl_detailpage(product_url),
        }
        records.append(data)
        print(product.get("name"))

records = []


def main():
    ## We can change category id here.
    category_id = "11896"
    offset = 0
    category_url = f'https://www.asos.com/api/product/search/v2/categories/{category_id}?channel=desktop-web&country=FR&currency=EUR&keyStoreDataversion=dup0qtf-35&lang=fr-FR&limit=72&offset={offset}&rowlength=4&store=FR&tst-search-sponsored-products=true'
    crawl_listpage(category_url)


main()
df = pandas.DataFrame(records)
df.to_excel("asos_details.xlsx", index=False)
