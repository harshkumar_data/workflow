import axios from "axios"
import { useEffect, useState } from "react";
import { useParams } from "react-router-dom"
import TitleCase from "../../components/case_converter";

export default function Questions(){
    let params = useParams();
    let subject = params.subject.replace(/-/g, '/')
    let standard = params.standard.split('-')[6]
    let title = `Class ${standard} of ${subject}`
    const [questions, SetQuestions] = useState(null);
    let recordsPerPage = 10;
    let indexofFirstRecord = 0;

    useEffect(() => {
        axios.get(`http://127.0.0.1:5000/${standard}/${subject}/?limit=${recordsPerPage}&offset=${indexofFirstRecord}`).then(response => {
            SetQuestions(response.data)
            
        }).catch(error => {
            console.log(error)
        })
    }, [])
    
    console.log(questions)

    return (
        <div className="container mt-4">
            <h1>{title}</h1>
            <div className="row g-4 bg-2">{
                questions.map((item, index) => {
                    return (
                        <div className="quiz" id={index}>
                            <div className="question">
                                {item.question}
                                
                                <div>
                                    {item}
                                </div>
                            </div>
                            <div className="answer">
                                {item.answer}
                            </div>
                        </div>
                    )
                })
            }
            </div>
        </div>
    )
}