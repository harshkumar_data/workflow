// import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom'


function Standards() {
    let static_string = 'MCQ Questions with Answers for class __STANDARD__';
    const standards = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12'];
    // const [category_names, SetCategoryNames] = useState([]);

    // const fetchnames = () => {
    //     SetCategoryNames(['Gk', 'English'])
    // }

    // useEffect(() => {
    //     fetchnames()
    // },[])
    return (
        <div className='container align-text-center pt-4 mt-4'>
            <div className='heading text-center mb-5'>
                <h1>
                    {'MCQ Questions with Answers'}
                </h1>
            </div>
            <br></br>
            <div className="row g-4 mt-6">
                {
                    standards.map((item, index) => {
                        let title = static_string.replace('__STANDARD__', item);
                        let slug = title.toLowerCase().replace(/ /g, '-') + "-subjects"
                        return (
                            <div className="col col-lg-3" key={index}>
                                <div className='p-3 border bg-light c-black'>
                                    <Link to={slug} id={item} className="text-decoration-none text-info">
                                        <p className='text-center mb-0'>{title}</p>
                                    </Link>
                                </div>
                            </div>
                        )
                    })
                }
            </div>
        </div>
    );

}

export default Standards