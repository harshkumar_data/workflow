from flask import Flask, request, jsonify
from flask_cors import CORS
import database_manager


app = Flask(__name__, static_folder='frontend/build')
CORS(app)

questions_collection = database_manager.get_connection('question_bank')
subjects_collection = database_manager.get_connection('all_subjects')

@app.route('/')
def hello():
    return {"message": "hello world!!!"}


@app.route('/questions/')
def get_questionsV1():
    return {"message": "questions"}
#     limit = int(request.args.get("limit"))
#     offset = int(request.args.get("offset"))
#     return jsonify(database_manager.get_records(questions_collection, offset, limit, {}))

@app.route('/<string:standard>/')
def get_subjects_name(standard):
    query = {"standard": standard}
    print(query)
    return jsonify(database_manager.get_records(subjects_collection, 0, 10, query))


@app.route('/<string:standard>/<string:subject>/')
def get_questions(standard, subject):
    print(type(standard), type(subject))
    limit = int(request.args.get("limit"))
    offset = int(request.args.get("offset"))
    query = {"standard": standard, "subject": subject}
    return jsonify(database_manager.get_records(questions_collection, offset, limit, query))

if __name__ == "__main__":
    app.run(debug=True, port = 8000, host="0.0.0.0")
