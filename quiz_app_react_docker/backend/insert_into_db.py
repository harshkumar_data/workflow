from pymongo import MongoClient
import pandas

def get_client():
    uri = 'mongodb://localhost:27017'
    return MongoClient(uri)['quiz_game']

db_client = get_client()
listpage_collection = db_client['question_bank']

input_file = pandas.read_excel('all_subjects.xlsx')
records = []
for count in range(len(input_file)):
    records.append({'standard': str(input_file.iloc[count]['Standard']).strip().replace('\ufeff', ''), 'subject': input_file.iloc[count]['Subject'].
strip().replace('\ufeff', '')})

listpage_collection.insert_many(records)