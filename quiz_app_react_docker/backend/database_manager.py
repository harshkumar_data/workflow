from pymongo import MongoClient


def get_client():
    # uri = 'mongodb://localhost:27017/'
    return MongoClient(host="test_mongodb", port=27017)


def get_connection(collection_name):
    client = get_client()
    db = client['quiz_game']
    return db[collection_name]


def get_records(collection, offset, limit, query):
    raw = collection.find(query,{'_id': 0}).skip(offset).limit(limit)
    return [record for record in raw]
