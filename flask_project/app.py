from flask import Flask, render_template, jsonify

app = Flask(__name__)


@app.route("/")
def hello_world():
    return "<p>Hello, World!</p>"


@app.route("/products")
def products():
    return "this is product page"


@app.route("/templates")
def html_page():
    return render_template('index.html')


@app.route('/json/<int:n>')
def multiply(n):
    c = n*2
    data = {
        "Results": {
            "Number": n,
            "Result": c,
            "Return": True
        }
    }
    return jsonify(data)


if __name__ == "__main__":
    app.run(debug=True, port=8000)
