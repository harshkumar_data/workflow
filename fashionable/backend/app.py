from flask import Flask, request, jsonify
from flask_restful import Api, Resource, reqparse
from flask_cors import CORS
import db_manager

app = Flask(__name__, static_url_path='', static_folder='frontend/build')
CORS(app)
api = Api(app)


@app.route('/products/')
def get_data():
    limit = int(request.args.get("limit"))
    offset = int(request.args.get("offset"))
    return jsonify(db_manager.get_records(offset, limit))

@app.route('/<string:category>/<string:sku>/')
def get_details(category, sku):
    return jsonify(db_manager.get_detail_record(sku))

if __name__ == "__main__":
    app.run(debug=True, port = '5001')
