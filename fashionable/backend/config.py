from typing_extensions import TypedDict
from dotenv import load_dotenv
import os


class Config(TypedDict):
    """Monogo user"""
    DB_USER: str

    """Monogo password"""
    DB_PASSWORD: str

    """Monogo db name"""
    DB_NAME: str

    """Monogo db collection"""
    DB_COLLECTION: str

APP_CONFIG: Config = {}

# Configuration necessary for the running the program
required_config_keys = [
    "DB_USER",
    "DB_PASSWORD",
    "DB_NAME",
    "DB_COLLECTION",
]


def load_env_config():
    load_dotenv()
    global APP_CONFIG
    for key in required_config_keys:
        APP_CONFIG[key] = os.environ.get(key)
        if APP_CONFIG[key] is None:
            raise Exception(f"Exception: Missing key {key} in config")
