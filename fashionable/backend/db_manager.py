from pymongo import MongoClient
from config import APP_CONFIG, load_env_config

load_env_config()


def get_client():
	uri = f'mongodb+srv://{APP_CONFIG["DB_USER"]}' + ':' + \
		f'{APP_CONFIG["DB_PASSWORD"]}@cluster0.jgvyx.mongodb.net/{APP_CONFIG["DB_NAME"]}?retryWrites=true&w=majority'
	return MongoClient(uri)


def get_connection():
	client = get_client()
	db = client[APP_CONFIG['DB_NAME']]
	return db[APP_CONFIG['DB_COLLECTION']]

def get_convertion(data):
	data['_id'] = str(data['_id'])
	return data

def get_total_documents(filter):
	return collection.count_documents(filter)

def get_records(offset, limit):
	filter = {'category': 'allen solly'}
	total_documents = get_total_documents(filter)
	records = collection.find(filter).skip(offset).limit(limit)
	json_data = {}
	all_records = []
	for record in records:
		all_records.append(get_convertion(record))
	json_data.update({"records": all_records, 'total_count': total_documents})
	return json_data


def get_detail_record(product_sku):
	records = collection.find({'sku': product_sku})
	product_detail = []
	for record in records:
		product_detail.append(get_convertion(record))
	return product_detail

collection = get_connection()
