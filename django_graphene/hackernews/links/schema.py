import graphene
from graphene_django import DjangoObjectType

from .models import links, products, Book


class LinkType(DjangoObjectType):
    class Meta:
        model = links
        fields = '__all__'

class productType(DjangoObjectType):
    class Meta:
        model = products

class BookType(DjangoObjectType): 
    class Meta:
        model = Book
        fields = "__all__"

class Query(graphene.ObjectType):
    Link = graphene.List(LinkType)

    Products = graphene.List(productType)

    all_books = graphene.List(BookType)
    book = graphene.Field(BookType, book_id=graphene.Int())

# resolve_[?] is same you define above
    def resolve_all_books(self, info, **kwargs):
        return Book.objects.all()

    def resolve_book(self, info, book_id):
        return Book.objects.get(pk=book_id)
    def resolve_Link(self, info, **kwargs):
        return links.objects.all()

    def resolve_Products(self, info, **kwargs):
        return products.objects.all()
    
class BookInput(graphene.InputObjectType):
    id = graphene.ID()
    title = graphene.String()
    author = graphene.String()
    year_published = graphene.String()
    review = graphene.Int() 

class CreateBook(graphene.Mutation):
    class Arguments:
        book_data = BookInput(required=True)

    book = graphene.Field(BookType)

    @staticmethod
    def mutate(root, info, book_data=None):
        book_instance = Book( 
            title=book_data.title,
            author=book_data.author,
            year_published=book_data.year_published,
            review=book_data.review
        )
        book_instance.save()
        return CreateBook(book=book_instance)

class LinkInput(graphene.InputObjectType):
    url = graphene.String()
    description = graphene.String()

class CreateLink(graphene.Mutation):
    class Arguments:
        link_data = LinkInput(required=True)

    link = graphene.Field(LinkType)

    @staticmethod
    def mutate(root, info, link_data=None):
        link_instance = links( 
            url=link_data.url,
            description=link_data.description,
        )
        link_instance.save()
        return CreateLink(link=link_instance)

class Mutation(graphene.ObjectType):
    create_book = CreateBook.Field()
    update_book = CreateLink.Field()
    # delete_book = DeleteBook.Field()


schema = graphene.Schema(query=Query, mutation=Mutation)