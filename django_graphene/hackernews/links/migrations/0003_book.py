# Generated by Django 2.1.4 on 2022-02-06 12:10

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('links', '0002_products'),
    ]

    operations = [
        migrations.CreateModel(
            name='Book',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=100)),
                ('author', models.CharField(max_length=50)),
                ('year_published', models.CharField(max_length=10)),
                ('review', models.PositiveIntegerField()),
            ],
        ),
    ]
