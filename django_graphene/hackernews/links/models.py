from django.db import models

# Create your models here.
class links(models.Model):
    url = models.TextField()
    description = models.TextField(blank=True)


class products(models.Model):
    name = models.CharField(max_length=200)
    price = models.FloatField()

class Book(models.Model):
    title = models.CharField(max_length=100)
    author = models.CharField(max_length=50)
    year_published = models.CharField(max_length=10)
    review = models.PositiveIntegerField()
    
    def __str__(self):
        return self.title 