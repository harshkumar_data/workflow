import React from 'react';
import './App.css';
import Barchart from './components/BarChart.js';

function App() {
  return (
    <div className="App">
      <Barchart />
    </div>
  );
}

export default App;