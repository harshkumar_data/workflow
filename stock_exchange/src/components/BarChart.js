import React from 'react'
import {
    Chart as ChartJS,
    CategoryScale,
    LinearScale,
    BarElement,
    Title,
    Tooltip,
    Legend,
    
} from 'chart.js'
import { Bar } from 'react-chartjs-2'


ChartJS.register(
    CategoryScale,
    LinearScale,
    BarElement,
    Title,
    Tooltip,
    Legend
)


const Barchart = () => {
    const labels = ['22/08', '23/08', '24/08', '25/08', '26/08', '27/08']
    const options = {
        plugins: {
            title: {
                display: true,
                text: `Stock`
            },
        }
    }

    const data = {
        labels,
        datasets: [
            {
                label: `Zomato `,
                data: [1,2,3,4,5,6,7,8,9,10],
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)'
                ],
                borderColor: [
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 1
            }
        ]
    }

    return (
        <div>
            <div>
                <Bar options={options} data={data} height={50} width={100}/>
            </div>
        </div>

    )

}

export default Barchart