import requests
import pandas as pd
import re

def get_headers(profile_url):
    cookies = {
        'bcookie': '"v=2&6309d20e-7549-4afd-843b-fa81de90a716"',
        'bscookie': '"v=1&2022060713335924318f56-bdfe-426f-8627-2b928d41657aAQGXDkG919KIzXH_W6PiPkhCehKzN5wn"',
        'li_rm': 'AQGRigGy6yt7agAAAYIvz6ITN_gDVYkVkoUiwA7voY2dAUv8l_SB2jB268da_ilZx5qZxPMpq1cW-E-KlpEvz0b1OikKSj7yx2KEFruu4-vEtRLlKBOmR-yE2Mg5v9u3ZVIiNeF_9UpH2YszEiNoUda5XbA00p0WJMGbk7gvyMeDCG64FximJa7OnN4vDtStZrogdHJ2JB-FQN9_S-cjMxWsL9Ti43BET8QVXuskroZaRrKGUYq0HvD6PleuArBc55TwukOigMZoB73ZYmkZ96tyaBt7HftjxqVTRj_kM_qWWwJL0xPlgVUKU3IXQpqx2rN85U9NT2fdGUHzSns',
        'G_ENABLED_IDPS': 'google',
        'AMCV_14215E3D5995C57C0A495C55%40AdobeOrg': '-637568504%7CMCIDTS%7C19221%7CMCMID%7C08006135266392602592245659608591030438%7CMCAAMLH-1661237160%7C12%7CMCAAMB-1661237160%7C6G1ynYcLPuiQxYZrsz_pkqfLG9yMXBpb2zX5dvJdYQJzPXImdj0y%7CMCOPTOUT-1660639560s%7CNONE%7CvVersion%7C5.1.1%7CMCCIDH%7C-434687713',
        'aam_uuid': '08521555878854168722298243977875303277',
        'timezone': 'Asia/Kolkata',
        'li_theme': 'light',
        'li_theme_set': 'app',
        'UserMatchHistory': 'AQIf6Yojs5C4uAAAAYKlZ5ukUoGu21d-vGyjIH8B3BH2iovFr4ZUwNAeBxf7BIOQorV7ch4a6WJVBIirTHEn_eoLWG4oK2C_psFKUXFDpDgOYEAvgNSopJ2xhQMI0f4pCF09WtohTeFRBdgDdhnwK-pvDT50_w86fbP7QaHdAEuY5mshO8cIYzdpMoqp2Do0t-zM3fanBg1BKQgQom710SjProukZMxu6UQnfuqKoJ2O0m5w-Grd0pfrGPiNEu4bktyAlbDHcTxKM0lUukqY6YC-LA7XpnrtpM-fSc0',
        'li_sugr': '478c97f3-40e9-4216-8889-67c6b118819b',
        '_guid': '351831f3-878f-42d4-91d3-169e41e55c07',
        'AnalyticsSyncHistory': 'AQKK6gpKHBpo4QAAAYKk9SLvU7KHz-wknVs0T4GuTR-dbmyuYoqR_XpDFI0DKKr9IJ0CQhPArI4roLzgjAKopw',
        'lms_ads': 'AQEuFbRb43nYOgAAAYKk9SS1PgoypiDTIs-x1JdZwGR-B37WVoWa3VqOu96QaoGFPFQ5eyPFiKyKJxcuEfVOYUM4agWOB70C',
        'lms_analytics': 'AQEuFbRb43nYOgAAAYKk9SS1PgoypiDTIs-x1JdZwGR-B37WVoWa3VqOu96QaoGFPFQ5eyPFiKyKJxcuEfVOYUM4agWOB70C',
        '_gcl_au': '1.1.1869563648.1654608861',
        'gpv_pn': 'mobile.linkedin.com%2F',
        's_tslv': '1658644366578',
        'pushPermState': 'default',
        'li_theme_set': 'app',
        'VID': 'V_2022_07_24_06_366',
        'mbox': 'session#7ede31704c1e41a480f77e24d5af0f5f#1658646227|PC#7ede31704c1e41a480f77e24d5af0f5f.31_0#1674196367',
        'visit': 'v=1&M',
        'g_state': '{"i_l":2,"i_p":1660718777279}',
        'fid': 'AQGYvKEmFEFvDwAAAYKJMDYy90W1yIMZjufsMwxu-VJ7QzaKnDecuJjqTiWTcuOHJj0zKy0tlbc3xA',
        'fcookie': 'AQHx2_-HJDPi8QAAAYKk9ryorIHLKMLVL6jYmPOUq44BvoK2LYe_lUBXLNqbxRYQNKv72MLFA-RwNYL6eXVePcO8PYLbDkxxeToyqjDDPv_VCnD18NWqaH5qV-XnP19X-orXwv9tNAJ_KsGzCbN7lHcehpbEvktZHy4zmAYiGGCfIn9nCjQEci9ky17y4DeMLBC35C-XytFpWQrNukqllsIBfkTKpEU29tROJU4h2X18TVuYC3RON4AzdxGXf+XMjlrNGr9HlfwUwQxJxEdLzBTgn4UNG0IJ9qVHMOWuHkWhJMrseVUm7WqsihxLXWOIUljnuSS9Kw==',
        'AMCVS_14215E3D5995C57C0A495C55%40AdobeOrg': '1',
        'sdsc': '1%3A1SZM1shxDNbLt36wZwCgPgvN58iw%3D',
        'lidc': '"b=TB89:s=T:r=T:a=T:p=T:g=2852:u=2:x=1:i=1660632409:t=1660636110:v=2:sig=AQHfczzXi8IcgoA83MnQKJHinfqKybUK"',
        'li_g_recent_logout': 'v=1&true',
        'JSESSIONID': '"ajax:8559132823047940543"',
        'lang': 'v=2&lang=en-us',
        'liap': 'true',
        'li_at': 'AQEDAT1W97kFpZYOAAABgqVnjb4AAAGCyXQRvk4ATeY-UyAzSTDMfR5HdyN7pH8tkDU33KfyoabhS43ci9pAVZIL4lackQZyGWkQpM4lNpxRZ-O5EZt3uyn7dMSlSO_nxWs5pX4llmR4n53Y6Xpw8QSm',
    }

    headers = {
        'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:103.0) Gecko/20100101 Firefox/103.0',
        'Accept': 'application/vnd.linkedin.normalized+json+2.1',
        'Accept-Language': 'en-US,en;q=0.5',
        'x-li-lang': 'en_US',
        'x-li-track': '{"clientVersion":"1.10.8767","mpVersion":"1.10.8767","osName":"web","timezoneOffset":5.5,"timezone":"Asia/Kolkata","deviceFormFactor":"DESKTOP","mpName":"voyager-web","displayDensity":1,"displayWidth":1600,"displayHeight":900}',
        'x-li-page-instance': 'urn:li:page:d_flagship3_profile_view_base;6AixdpU9SqydM0/iLarbgQ==',
        'csrf-token': 'ajax:8559132823047940543',
        'x-restli-protocol-version': '2.0.0',
        'x-li-pem-metadata': 'Voyager - Profile=profile-cards-deferred',
        'Connection': 'keep-alive',
        'Referer': profile_url,
        'Sec-Fetch-Dest': 'empty',
        'Sec-Fetch-Mode': 'cors',
        'Sec-Fetch-Site': 'same-origin',
    }
    return cookies, headers

def get_skills(profile_url, profile_id):
    cookies, headers = get_headers(profile_url)
    response = requests.get(f'https://www.linkedin.com/voyager/api/graphql?variables=(profileUrn:urn%3Ali%3Afsd_profile%3A{profile_id},sectionType:skills,locale:en_US)&&queryId=voyagerIdentityDashProfileComponents.4d3ec80f0f977cddbf91821bcc641fdd', cookies=cookies, headers=headers)
    js = response.json()
    raw_skills = js.get('included')[1].get('components').get('elements')
    skill_list = []
    for skill in raw_skills:
        skill_list.append(skill.get('components').get('entityComponent').get('title').get('text'))
    all_skills = '| '.join(skill_list)
    return all_skills


def get_contact_info(profile_url):
    cookies, headers = get_headers(profile_url)
    url_mid = profile_url.split('/in/')[1]
    response = requests.get(f'https://www.linkedin.com/voyager/api/identity/profiles/{url_mid}profileContactInfo', cookies=cookies, headers=headers)
    js = response.json()
    phone_no = js.get('data').get('phoneNumbers')
    email_address = js.get('data').get('emailAddress')
    return phone_no, email_address

def get_profile_id(profile_url):
    cookies, headers = get_headers(profile_url)
    response = requests.get(profile_url, cookies=cookies, headers=headers)    
    return re.search('fsd_profileCard:(.*?),EXPERIENCE', response.text).group(1).replace('(','')


def crawl_profile(profile_name, profile_url):
    cookies, headers = get_headers(profile_url)
    profile_id = get_profile_id(profile_url)
    response = requests.get(f'https://www.linkedin.com/voyager/api/graphql?variables=(profileUrn:urn%3Ali%3Afsd_profile%3A{profile_id},locale:(language:en,country:US))&&queryId=voyagerIdentityDashProfileCards.ad1403014f24b21006a9830b38247379', cookies=cookies, headers=headers)
    js = response.json()
    if js.get('included')[13].get('topComponents'):
        experiences = js.get('included')[13].get('topComponents')[1].get('components').get('fixedListComponent').get('components')
    else:
        experiences = js.get('included')[4].get('topComponents')[1].get('components').get('fixedListComponent').get('components')
    experience_details = {}
    count = 1
    for exp in experiences:
        experience_details[f'Company Name {count}'] = exp.get('components').get('entityComponent').get('subtitle').get('text')
        experience_details[f'Designation {count}'] = exp.get('components').get('entityComponent').get('title').get('text')
        experience_details[f'Working Period {count}'] = exp.get('components').get('entityComponent').get('caption').get('text')
        experience_details[f'Company Location {count}'] = exp.get('components').get('entityComponent').get('metadata').get('text')
        count += 1

    phone_no, email_id = get_contact_info(profile_url)
    main = {}
    main['Profile Url'] = profile_url
    main['Profile Name'] = profile_name
    main['Skills'] = get_skills(profile_url, profile_id)
    main['Contact No'] = phone_no
    main['Email Id'] = email_id
    main.update(experience_details)
    profile_data.append(main)
    print(main)

profile_data = []
profiles = {'Harsh Kumar': 'https://www.linkedin.com/in/harsh-kumar-27022a176/', 'Devender Kumar': 'https://www.linkedin.com/mwlite/in/devender-kumar-b67838190'}
for profile in profiles:
    for i in range(800):
        try:
            crawl_profile(profile, profiles[profile])
            print('!!!!!!!!!', i)
        except:
            pass

df = pd.DataFrame(profile_data)
df.to_excel('test_Again.xlsx')