import requests
from lxml import html
import pandas as pd


def get_headers():
    cookies = {
        'bcookie': '"v=2&6309d20e-7549-4afd-843b-fa81de90a716"',
        'bscookie': '"v=1&2022060713335924318f56-bdfe-426f-8627-2b928d41657aAQGXDkG919KIzXH_W6PiPkhCehKzN5wn"',
        'li_rm': 'AQGRigGy6yt7agAAAYIvz6ITN_gDVYkVkoUiwA7voY2dAUv8l_SB2jB268da_ilZx5qZxPMpq1cW-E-KlpEvz0b1OikKSj7yx2KEFruu4-vEtRLlKBOmR-yE2Mg5v9u3ZVIiNeF_9UpH2YszEiNoUda5XbA00p0WJMGbk7gvyMeDCG64FximJa7OnN4vDtStZrogdHJ2JB-FQN9_S-cjMxWsL9Ti43BET8QVXuskroZaRrKGUYq0HvD6PleuArBc55TwukOigMZoB73ZYmkZ96tyaBt7HftjxqVTRj_kM_qWWwJL0xPlgVUKU3IXQpqx2rN85U9NT2fdGUHzSns',
        'G_ENABLED_IDPS': 'google',
        'AMCV_14215E3D5995C57C0A495C55%40AdobeOrg': '-637568504%7CMCIDTS%7C19198%7CMCMID%7C08006135266392602592245659608591030438%7CMCAAMLH-1659264511%7C12%7CMCAAMB-1659264511%7C6G1ynYcLPuiQxYZrsz_pkqfLG9yMXBpb2zX5dvJdYQJzPXImdj0y%7CMCOPTOUT-1658666911s%7CNONE%7CvVersion%7C5.1.1%7CMCCIDH%7C-241015803',
        'aam_uuid': '08521555878854168722298243977875303277',
        'timezone': 'Asia/Kolkata',
        'li_theme': 'light',
        'li_theme_set': 'app',
        'UserMatchHistory': 'AQK51-U4ZQTM5QAAAYIv1sckutXqCOWvAU-nqHBqFcUQVTv6h5Ic_1QtlshN-vi7aD8DwGst24EL_cdcrMZSq13jEH9kQkndp_dtNx9pmDBqadvDrlTZZlie3cOkEth93IxChnuw0SMHTR4ggvRh4th3Fuy1nSp6Hf1Hi0TOCFFgu3kg4BJajcHqMUS9pLxqwhsIv0NZJPUNTYcCoFXqiU3yoCLNAxWXBrQs6qN1Ks77aztkYZtU4-lfiCvzVNi7Qtqa2SVvReCrUZHFxgofyy3qcI443nCu053Agls',
        'li_sugr': '478c97f3-40e9-4216-8889-67c6b118819b',
        '_guid': '351831f3-878f-42d4-91d3-169e41e55c07',
        'AnalyticsSyncHistory': 'AQLsjy_uwi05iQAAAYIlXs6sDXjGmt9Pzwu1dTBDY4Ns9PMtWJgR8x9wg8-CMWA3YfF7PVF9kJ9a5fx6ULy6fQ',
        'lms_ads': 'AQF5PhqSlGXn-wAAAYIlXtQWwmIK5UNWfJaegmpaHRAZo7ak3ebJS6tpFKHLDs7aLZP8MCENRIVksNS0c7n-0NcgsLcRz9gD',
        'lms_analytics': 'AQF5PhqSlGXn-wAAAYIlXtQWwmIK5UNWfJaegmpaHRAZo7ak3ebJS6tpFKHLDs7aLZP8MCENRIVksNS0c7n-0NcgsLcRz9gD',
        '_gcl_au': '1.1.1869563648.1654608861',
        'gpv_pn': 'mobile.linkedin.com%2F',
        's_tslv': '1658644366578',
        'pushPermState': 'default',
        'li_theme_set': 'app',
        'VID': 'V_2022_07_24_06_366',
        'mbox': 'session#7ede31704c1e41a480f77e24d5af0f5f#1658646227|PC#7ede31704c1e41a480f77e24d5af0f5f.31_0#1674196367',
        'lidc': '"b=VP00:s=V:r=V:a=V:p=V:g=206:u=2:x=1:i=1658659985:t=1658660226:v=2:sig=AQH5dR1rj_U0RimmtjuhA-7f6elUmZf4"',
        'li_g_recent_logout': 'v=1&true',
        'visit': 'v=1&M',
        'JSESSIONID': '"ajax:6928765156993631435"',
        'lang': 'v=2&lang=en-us',
        'AMCVS_14215E3D5995C57C0A495C55%40AdobeOrg': '1',
        'li_at': 'AQEDATy2XxcEdCWXAAABgi_SQg0AAAGCU97GDU0AfolwFLet9p6BzaY4wha-MHfTIAxs0Ei4SEvjAmpHb2bxDiIu0e9OU5bN_cdK2h_VGX8rAlQ-1TB7Mkj8LYqRY3as3cBXG8MA_xQcohAwE0nF1d8w',
        'liap': 'true',
    }
    headers = {
        'User-Agent': 'Mozilla/5.0 (iPhone; CPU iPhone OS 14_6 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/14.0.3 Mobile/15E148 Safari/604.1',
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8',
        'Accept-Language': 'en-US,en;q=0.5',
        'Connection': 'keep-alive',
        'Upgrade-Insecure-Requests': '1',
        'Sec-Fetch-Dest': 'document',
        'Sec-Fetch-Mode': 'navigate',
        'Sec-Fetch-Site': 'none',
        'Sec-Fetch-User': '?1',
    }
    return cookies, headers


def crawl_profiles(profile_url):
    cookies, headers = get_headers()
    response = requests.get(profile_url, cookies=cookies, headers=headers)
    tree = html.fromstring(response.text)
    experiences = tree.xpath('//section[@class="experience-container list-container collapsable-list-container false false false"]//li[@class="entity-lockup profile-entity-lockup grouped "] | //section[@class="experience-container list-container collapsable-list-container   false"]//li[@class="entity-lockup profile-entity-lockup grouped "]')
    experience_details = {}
    count = 1
    for exp in experiences:
        if exp.xpath('.//a[@class="timeline-head"]'):
            head_count = 1
            for timeline in exp.xpath('.//div[@class="experience-roles-container collapsable-list-container"]//li'):
                experience_details[f'Company Name {count} (head_{head_count})'] = ''.join(exp.xpath('.//a[@class="timeline-head"]//div[@class="large-semibold list-item-heading"]//text()')).strip()
                experience_details[f'Designation {count} (head_{head_count})'] = ''.join(
                    timeline.xpath('.//dt[@class="medium-semibold"]//text()')).strip()
                experience_details[f'Working Period {count} (head_{head_count})'] = ' '.join(
                    timeline.xpath('.//dd[@class="medium time-period"]//span//text()')).strip()
                experience_details[f'Company Location {count} (head_{head_count})'] = ''.join(
                    timeline.xpath('.//dd[@class="small-light entity-location"]//text()')).strip()
                head_count += 1
        else:
            experience_details[f'Company Name {count}'] = ''.join(
                exp.xpath('.//dd[@class="medium entity-name"]//text()')).strip()
            experience_details[f'Designation {count}'] = ''.join(
                exp.xpath('.//dt[@class="large-semibold list-item-heading"]//text()')).strip()
            experience_details[f'Working Period {count}'] = ' '.join(
                exp.xpath('.//dd[@class="medium time-period"]//span//text()')).strip()
            experience_details[f'Company Location {count}'] = ''.join(
                exp.xpath('.//dd[@class="small-light entity-location"]//text()')).strip()
        count += 1

    skills = '| '.join(tree.xpath(
        '//li[@class="skill-item body-small-bold"]//span//text()')).strip()
    contact_info = tree.xpath(
        '//section[@class="contacts-container collapsable-list-container list-container"]')
    contacts = {}
    for contact in contact_info:
        contacts[''.join(contact.xpath('.//li[@class="contact-info email"]//p//text()')).strip()
                 ] = ''.join(contact.xpath('.//li[@class="contact-info email"]//a//text()')).strip()
        contacts[''.join(contact.xpath('.//li[@class="contact-info"]//p[@class="contact-title large-semibold phone"]//text()')
                         ).strip()] = ''.join(contact.xpath('.//li[@class="contact-info"]//ul[@class="contact-value "]//li//text()')).strip()

    profile_name = ''.join(tree.xpath(
        '//dt[@class="member-name extra-extra-large-semibold"]//span[@dir="ltr"]//text()')).strip()
    working_at = ''.join(tree.xpath(
        '//dl[@class="member-description"]//dd[@class="medium"]//text()')).strip()
    main = {}
    main['Profile Url'] = response.url
    main['Profile Name'] = profile_name
    main['Working Status'] = working_at
    main['Skills'] = skills
    main.update(contacts)
    main.update(experience_details)
    profile_data.append(main)
    print(response.url)
    print(main)

profile_data = []
urls = ['https://www.linkedin.com/mwlite/in/urvashi-yadav-049b1912a', 'https://www.linkedin.com/in/harsh-kumar-27022a176/', 'https://www.linkedin.com/mwlite/in/devender-kumar-b67838190']
for url in urls:
    for i in range(1000):
        crawl_profiles(url)
        print('!!!!!!!!!!!!!!!', i)
        
df = pd.DataFrame(profile_data)
df.to_excel('test.xlsx')
