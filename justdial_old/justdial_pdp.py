from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import subprocess
import time
import keyboard
from PIL import ImageGrab
import pymsgbox
import pytesseract
import cv2
import ctypes
from bs4 import BeautifulSoup as BS
import csv
import uuid
input = open('just_links07.csv','r').read().split('\n')

f = open('detail.csv','w',encoding='utf-8',newline='')
row = csv.writer(f)

pytesseract.pytesseract.tesseract_cmd=r"C:\\Program Files\Tesseract-OCR\\tesseract.exe"
chrmprof = "C:\\Users\\HP\\AppData\\Local\\Google\\Chrome\\User Data\\Default"
subprocess.call('start chrome.exe -remote-debugging-port=9014 --user-data-dir-' + chrmprof,shell=True)
chrome_options = Options()
ch_path = 'C:/Users/HP/Downloads/chromedriver_win32/chromedriver.exe'
chrome_options.add_experimental_option("debuggerAddress","localhost:9014")
driver = webdriver.Chrome(chrome_options=chrome_options,executable_path=ch_path)
url = "https://www.justdial.com/Delhi/Nixis-Institute-Jahangir-Puri/011PXX11-XX11-180608070047-R8H8_BZDET"
i = 1

def re_crawl(driver,url):
    driver.get(url)
    soup = BS(driver.page_source,'html.parser')
    if(soup.find("title").text.lower() == "error"):
        driver.get("https://www.justdial.com/")
        re_crawl(driver,url)
    else:
        soup = BS(driver.page_source,'html.parser')
        return soup
def crawling(url):
    try:
        global i
        # driver.get(url)
        soup = re_crawl(driver,url)
        # soup = BS(driver.page_source,'html.parser')
        # if(soup.find("title").text.lower() == "error"):
        #     re_crawl(driver,url)
        time.sleep(1.5)
        image = ImageGrab.grab()
        croped = image.crop((195,250,500,290))
        croped.save('images/'+str(i)+".jpg",quality=100)
        # pymsgbox.alert("ScreenShot Saved",title="Justdial")
        print('ScreenShot Saved'+'*******'+str(i))
        img = cv2.imread('images/'+str(i)+".jpg")
        txt = pytesseract.image_to_string(img).strip()
        address = ''.join(soup.find('span','adrstxtr').find('span').text).strip()
        name = ''.join(soup.find('h1','rstotle').find('span','fn').text).strip()
        row.writerow([url,name,address,txt])
        i = i+1
    except:
        crawling(url)

for url in input[27:150]:
    crawling(url)
ctypes.windll.user32.MessageBoxW(0, "ScreenShot Done", "ScreenShot", 1)