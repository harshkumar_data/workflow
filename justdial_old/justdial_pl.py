from requests import Session
from bs4 import BeautifulSoup as BS
import json
import csv

# ll = []
ss = Session()
f = open('just_links07.csv','w',newline="",encoding='utf-8')
row = csv.writer(f)

ss.headers={"Accept":"application/json, text/javascript, */*; q=0.01",
"Accept-Language":"en-US,en;q=0.5",
"Connection":"keep-alive",
"Host":"www.justdial.com",
"User-Agent":"Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:89.0) Gecko/20100101 Firefox/89.0",
"X-FRSC-Token":"ab7e0c3944fe487dc3b771bf65ae08f2c756b70443d8bd029e3a4ba6fddbfd17",
"X-Requested-With":"XMLHttpRequest"}

area_code = ['110006','110007','110002']

# ll = []
for area in area_code:
    i = 1
    ss.headers["Cookie"]="ppc=; PHPSESSID=2c6ca269c6faa5b3f583872807645e55; TKY=ab7e0c3944fe487dc3b771bf65ae08f2c756b70443d8bd029e3a4ba6fddbfd17; _ctok=7008e9287b0b4b326ee1ad6572bc7c87e364f8040e22c869b8eac04a969dd794; main_city=Delhi; attn_user=logout; ppc=; _abck=E004BA5327E1D60B254D4C9B1D00C265~0~YAAQNSEPF+UahOR5AQAAjAjC7gaF7SLTKW1OIuuZHmoywGinVHmsMMOzMEukHa2V3djT4bqc7HXyKvoyxhqWhkd7Gv2/927Qfde83YiBcnawBojFQdPp030aPXDxY5Nix0qwdW03Ue2nRv1I3a11SB4KH7/CWtfxhaiAnBnRhvvcS5uHHIOLxCNx2v48JZqUTKz+hSwEGMU4wC2wBG8oT5lWeFZY5OyMHCt/fPiA+g/HNc…W4ELtj/DhQFGgE3rk82+kH77EcZ8tU/CQ1HcPix/GqABGoHOEf4l0ZbMzRfrvhO68FLYj6YDZhaQJ03O5WXq1qBaNmQCUJFRzFQ4yjpCYjdriSdsd04psNefU3vc/0WrLoy0RvA2NA5FSA9fU+/h54=; bm_mi=CD9E9FA5C1069CB4862B13BF4C979028~Fs0tQZx0zoPm/l9Pnu+qNiiQZut/78XL7Tpx5hieni8Ple5/PKmIixWlJPR1NakTFZ8dN2Mlv1rYxS+K0L3IF/CUbxYvkflLu2I/qUURaWu24qzdeEomgPdmrDBFjDspo1tEIOYm033ZbIq+Ws+cs+SY6LlGCxd+xMiYkcX4TsE3Iq+IkflKdeTX5SrJhlSP3G9vyUwLA8ljhMiQxgCSk3/zZgiDr0c7RqVnHUMuGgwiTYO6I4jepXOFLnYZWIHz; prevcatid=10422444; bd_inputs=7|6|Schools; search_area={}".format(area).encode('utf-8')
    ss.headers['Referer']='https://www.justdial.com/Delhi/Schools-in-{}'.format(area)
    main_url = 'https://www.justdial.com/Delhi/Schools-in-{}'.format(area)
    # main_url = 'https://www.justdial.com/Delhi/Schools-in-110007'
    m_r = ss.get(main_url)
    m_soup = BS(m_r.content,'html.parser')
    m_links = m_soup.find_all("a","morehvr")
    for l in m_links:
        # ll.append(l.get('href'))
        row.writerow([l.get('href')])
    url = 'https://www.justdial.com/functions/ajxsearch.php?national_search=0&act=pagination_new&city=Delhi&search=Schools&where={}&catid=0&psearch=&prid=&page={}&SID=&mntypgrp=0&toknbkt=&bookDate=&jdsrc=&median_latitude=28.654657000000&median_longitude=77.225452000000&ncatid=10422444&mncatname=Schools&dcity=Delhi&pncode={}&htlis=0'
    # url = 'https://www.justdial.com/functions/ajxsearch.php?national_search=0&act=pagination_new&city=Delhi&search=Schools&where=110007&catid=0&psearch=&prid=&page={}&SID=&mntypgrp=0&toknbkt=&bookDate=&jdsrc=&median_latitude=28.654657000000&median_longitude=77.225452000000&ncatid=10422444&mncatname=Schools&dcity=Delhi&pncode=110007&htlis=0'
    try:
        while True:
            r = ss.get(url.format(area,i,area))
            print(r.url)
            js = json.loads(r.text)
            if(js['execution']==None):
                pass
            else:
                if(js['markup']):
                    soup = BS(js['markup'],'html.parser')
                    links = soup.find_all("a","morehvr")
                    for link in links:
                        # ll.append(link.get('href'))
                        row.writerow([link.get('href')])
                else:
                    break
            i = i+1
            print(i)
    except:
        pass
    print(area)

address = ''.join(soup.find('span','adrstxtr').find('span').text).strip()
name = ''.join(soup.find('h1','rstotle').find('span','fn').text).strip()