import { BaseEntity, Column, Entity, PrimaryGeneratedColumn } from "typeorm";


@Entity('usersV2')
export class User extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    first_name: string;

    @Column()
    last_name: string;

    @Column()
    age: number;
}
