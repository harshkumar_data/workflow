## Description

[Nest](https://github.com/nestjs/nest) framework TypeScript starter repository.

## Installation

```bash
$ sudo npm i -g @nestjs/cli
$ nest new <project-name>
$ sudo npm install i --save @nestjs/config
sudo npm install i --save @nestjs/mongoose mongoose
```
## To create new resource
```bash 
$ nest g res <resource-name>
```
## NestJs MongoDB Connection Source Video
[NestJs MongoDB In Hindi Explained](https://www.youtube.com/watch?v=R6Qz5zJLTe0&ab_channel=Notezz)

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```
